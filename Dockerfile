FROM alpine:3.15.0

ARG ANDROID_COMPILE_SDK="31"
ARG ANDROID_BUILD_TOOLS="31.0.0"
ARG ANDROID_SDK_TOOLS="4333796"

#  - Install necessary packages for android build
RUN apk update
RUN apk add gcompat openjdk8 openjdk11

#  - Change default jvm to jdk 8
RUN ln -nsf /usr/lib/jvm/java-8-openjdk /usr/lib/jvm/default-jvm

#  - Download android sdk tools
RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
RUN unzip -d android-sdk-linux android-sdk.zip

#  - Install necessary android build tools
RUN echo y | android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" > /dev/null
RUN echo y | android-sdk-linux/tools/bin/sdkmanager "platform-tools" > /dev/null
RUN echo y | android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
ENV ANDROID_HOME=$PWD/android-sdk-linux
RUN yes | android-sdk-linux/tools/bin/sdkmanager --licenses

# Set Java home for android buid
ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk

WORKDIR /app
COPY . .
ENTRYPOINT [ "./gradlew" ]
