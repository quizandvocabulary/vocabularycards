# About
This is simple vocabulary and quiz app for android written in kotlin

# Download
## Option 1
Build it in your local machine like other Android projects
## Option 2
Download artifact from gitlab ci/cd pipeline
## Option 3
Install it using this link https://play.google.com/store/apps/details?id=me.asddsajpgnoob.quizvocabulary
