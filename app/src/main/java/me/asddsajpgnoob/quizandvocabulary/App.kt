package me.asddsajpgnoob.quizandvocabulary

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class App : Application() {

    companion object {
        lateinit var INSTANCE: App
            private set
    }

    val appScope = CoroutineScope(SupervisorJob())

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }
}