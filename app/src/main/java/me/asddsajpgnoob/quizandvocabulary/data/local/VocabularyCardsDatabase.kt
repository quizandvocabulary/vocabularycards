package me.asddsajpgnoob.quizandvocabulary.data.local

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import me.asddsajpgnoob.quizandvocabulary.App
import me.asddsajpgnoob.quizandvocabulary.BuildConfig
import me.asddsajpgnoob.quizandvocabulary.data.local.dao.VocabularyCardDao
import me.asddsajpgnoob.quizandvocabulary.data.model.room.VocabularyCardEntity

@Database(
    version = 1,
    entities = [
        VocabularyCardEntity::class
    ]
)
abstract class VocabularyCardsDatabase : RoomDatabase() {

    companion object {
        private var INSTANCE: VocabularyCardsDatabase? = null

        fun get() = INSTANCE ?: synchronized(this) {
            Room.databaseBuilder(App.INSTANCE, VocabularyCardsDatabase::class.java, BuildConfig.DATABASE_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
                .also {
                    INSTANCE = it
                }
        }
    }

    abstract fun vocabularyCardDao(): VocabularyCardDao
}