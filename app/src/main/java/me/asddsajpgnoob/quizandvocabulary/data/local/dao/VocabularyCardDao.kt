package me.asddsajpgnoob.quizandvocabulary.data.local.dao

import androidx.room.*
import me.asddsajpgnoob.quizandvocabulary.data.model.room.VocabularyCardEntity

@Dao
interface VocabularyCardDao {

    @Query("SELECT * FROM vocabulary_cards ORDER BY datetime(created_at) DESC LIMIT :limit OFFSET :offset")
    suspend fun findAllOrderByCreatedAtDesc(offset: Int, limit: Int): List<VocabularyCardEntity>

    @Query("SELECT * FROM vocabulary_cards ORDER BY datetime(created_at) ASC LIMIT :limit OFFSET :offset")
    suspend fun findAllOrderByCreatedAtAsc(offset: Int, limit: Int): List<VocabularyCardEntity>

    @Query("SELECT * FROM vocabulary_cards WHERE origin LIKE '%' || :query || '%' OR translation LIKE '%' || :query || '%' ORDER BY datetime(created_at) DESC LIMIT :limit OFFSET :offset")
    suspend fun searchByOriginOrTranslationOrderByCreatedAtDesc(query: String, offset: Int, limit: Int): List<VocabularyCardEntity>

    @Query("SELECT * FROM vocabulary_cards WHERE id NOT IN (:exceptionIds) AND show_in_quiz is 1 ORDER BY RANDOM() LIMIT 1")
    suspend fun findRandomWithExceptions(exceptionIds: List<Long>): VocabularyCardEntity?

    @Query("SELECT * FROM vocabulary_cards ORDER BY not_guessed_count DESC LIMIT :limit OFFSET :offset")
    suspend fun findAllOrderByLessGuessedDesc(offset: Int, limit: Int): List<VocabularyCardEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(card: VocabularyCardEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(card: VocabularyCardEntity)

    @Delete
    suspend fun delete(card: VocabularyCardEntity)
}