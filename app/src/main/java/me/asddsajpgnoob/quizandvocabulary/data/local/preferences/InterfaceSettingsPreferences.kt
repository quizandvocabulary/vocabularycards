package me.asddsajpgnoob.quizandvocabulary.data.local.preferences

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import me.asddsajpgnoob.quizandvocabulary.App
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabulary.util.emitEmptyIfIOException

object InterfaceSettingsPreferences {

    private object InterfaceSettingsKeys {
        val DARK_MODE = stringPreferencesKey("darkMode")
        val DEFAULT_TAB = stringPreferencesKey("defaultTab")
        val SHOW_AUDIOS_IN_VOCABULARY_SCREEN = booleanPreferencesKey("showAudiosInVocabularyScreen")
        val SHOW_EXAMPLES_IN_VOCABULARY_SCREEN =
            booleanPreferencesKey("showExamplesInVocabularyScreen")
        val OPEN_TRANSLATOR_ON_TOP_OF_APP = booleanPreferencesKey("openTranslatorOnTopOfApp")
        val SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB =
            booleanPreferencesKey("saveNavigationStateWhenSwitchingTab")
    }

    private val Context.interfaceSettingsDataStore: DataStore<Preferences> by preferencesDataStore(
        name = "interface_settings_preferences"
    )

    private val DEFAULT_DARK_MODE = DarkModePreferences.FOLLOW_SYSTEM
    private val DEFAULT_DEFAULT_TAB = DefaultTabPreferences.QUIZ
    private const val DEFAULT_SHOW_AUDIOS_IN_VOCABULARY_SCREEN = false
    private const val DEFAULT_SHOW_EXAMPLES_IN_VOCABULARY_SCREEN = true
    private const val DEFAULT_OPEN_TRANSLATOR_ON_TOP_OF_APP = false
    private const val DEFAULT_SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB = true

    val darkMode: Flow<DarkModePreferences> = App.INSTANCE.interfaceSettingsDataStore.data
        .emitEmptyIfIOException()
        .map { preferences ->
            preferences[InterfaceSettingsKeys.DARK_MODE]?.let {
                DarkModePreferences.findByPreferenceName(it)
            } ?: DEFAULT_DARK_MODE
        }

    val defaultTab: Flow<DefaultTabPreferences> =
        App.INSTANCE.interfaceSettingsDataStore.data
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[InterfaceSettingsKeys.DEFAULT_TAB]?.let {
                    DefaultTabPreferences.findByPreferenceName(it)
                } ?: DEFAULT_DEFAULT_TAB
            }

    val showAudiosInVocabularyScreen: Flow<Boolean> =
        App.INSTANCE.interfaceSettingsDataStore.data
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[InterfaceSettingsKeys.SHOW_AUDIOS_IN_VOCABULARY_SCREEN]
                    ?: DEFAULT_SHOW_AUDIOS_IN_VOCABULARY_SCREEN
            }

    val showExamplesInVocabularyScreen: Flow<Boolean> =
        App.INSTANCE.interfaceSettingsDataStore.data
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[InterfaceSettingsKeys.SHOW_EXAMPLES_IN_VOCABULARY_SCREEN]
                    ?: DEFAULT_SHOW_EXAMPLES_IN_VOCABULARY_SCREEN
            }

    val openTranslatorOnTopOfApp: Flow<Boolean> = App.INSTANCE.interfaceSettingsDataStore.data
        .emitEmptyIfIOException()
        .map { preferences ->
            preferences[InterfaceSettingsKeys.OPEN_TRANSLATOR_ON_TOP_OF_APP]
                ?: DEFAULT_OPEN_TRANSLATOR_ON_TOP_OF_APP

        }

    val saveNavigationStateWhenSwitchingTab: Flow<Boolean> =
        App.INSTANCE.interfaceSettingsDataStore.data
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[InterfaceSettingsKeys.SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB]
                    ?: DEFAULT_SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB
            }

    suspend fun setDarkMode(darkMode: DarkModePreferences) {
        App.INSTANCE.interfaceSettingsDataStore.edit { preferences ->
            preferences[InterfaceSettingsKeys.DARK_MODE] = darkMode.preferenceName
        }
    }

    suspend fun setDefaultTab(defaultTab: DefaultTabPreferences) {
        App.INSTANCE.interfaceSettingsDataStore.edit { preferences ->
            preferences[InterfaceSettingsKeys.DEFAULT_TAB] = defaultTab.preferenceName
        }
    }

    suspend fun setShowAudiosInVocabularyScreen(show: Boolean) {
        App.INSTANCE.interfaceSettingsDataStore.edit { preferences ->
            preferences[InterfaceSettingsKeys.SHOW_AUDIOS_IN_VOCABULARY_SCREEN] = show
        }
    }

    suspend fun setExamplesAudiosInVocabularyScreen(show: Boolean) {
        App.INSTANCE.interfaceSettingsDataStore.edit { preferences ->
            preferences[InterfaceSettingsKeys.SHOW_EXAMPLES_IN_VOCABULARY_SCREEN] =
                show
        }
    }

    suspend fun setOpenTranslatorOnTopOfApp(open: Boolean) {
        App.INSTANCE.interfaceSettingsDataStore.edit { preferences ->
            preferences[InterfaceSettingsKeys.OPEN_TRANSLATOR_ON_TOP_OF_APP] = open
        }
    }

    suspend fun setSaveNavigationStateWhenSwitchingTab(save: Boolean) {
        App.INSTANCE.interfaceSettingsDataStore.edit { preferences ->
            preferences[InterfaceSettingsKeys.SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB] = save
        }
    }
}