package me.asddsajpgnoob.quizandvocabulary.data.local.preferences

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import me.asddsajpgnoob.quizandvocabulary.App
import me.asddsajpgnoob.quizandvocabulary.util.emitEmptyIfIOException

object OnTimeActionPreferences {

    private object OnTimeActionKeys {
        val SHOW_QUIZ_TOUCH_HINT = booleanPreferencesKey("showQuizTouchHint")
        val SHOW_QUIZ_SWIPE_DIRECTION_HINT = booleanPreferencesKey("showQuizSwipeDirectionHint")
    }

    private const val DEFAULT_SHOW_QUIZ_TOUCH_HINT = true
    private const val DEFAULT_SHOW_QUIZ_SWIPE_DIRECTION_HINT = true

    private val Context.onTimeActionPreferences: DataStore<Preferences> by preferencesDataStore(
        name = "on_time_action_preferences"
    )

    val showQuizTouchHint: Flow<Boolean> = App.INSTANCE.onTimeActionPreferences.data
        .emitEmptyIfIOException()
        .map { preferences ->
            preferences[OnTimeActionKeys.SHOW_QUIZ_TOUCH_HINT]
                ?: DEFAULT_SHOW_QUIZ_TOUCH_HINT
        }

    val showQuizSwipeDirectionHint: Flow<Boolean> =
        App.INSTANCE.onTimeActionPreferences.data
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[OnTimeActionKeys.SHOW_QUIZ_SWIPE_DIRECTION_HINT]
                    ?: DEFAULT_SHOW_QUIZ_SWIPE_DIRECTION_HINT
            }

    suspend fun setShowQuizTouchHint(show: Boolean) {
        App.INSTANCE.onTimeActionPreferences.edit { preferences ->
            preferences[OnTimeActionKeys.SHOW_QUIZ_TOUCH_HINT] = show
        }
    }

    suspend fun setShowQuizSwipeDirectionHint(show: Boolean) {
        App.INSTANCE.onTimeActionPreferences.edit { preferences ->
            preferences[OnTimeActionKeys.SHOW_QUIZ_SWIPE_DIRECTION_HINT] = show
        }
    }
}