package me.asddsajpgnoob.quizandvocabulary.data.local.preferences

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import me.asddsajpgnoob.quizandvocabulary.App
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.QuizModePreferences
import me.asddsajpgnoob.quizandvocabulary.util.emitEmptyIfIOException

object QuizSettingsPreferences {

    object QuizSettingsKeys {
        val QUIZ_MODE = stringPreferencesKey("quizMode")
    }

    private val Context.quizSettingsDataStore: DataStore<Preferences> by preferencesDataStore(
        name = "quiz_settings_preferences"
    )

    private val DEFAULT_QUIZ_MODE = QuizModePreferences.RANDOM

    val quizMode: Flow<QuizModePreferences> = App.INSTANCE.quizSettingsDataStore.data
        .emitEmptyIfIOException()
        .map { preferences ->
            preferences[QuizSettingsKeys.QUIZ_MODE]?.let {
                QuizModePreferences.findByPreferenceName(it)
            } ?: DEFAULT_QUIZ_MODE
        }

    suspend fun setQuizMode(quizMode: QuizModePreferences) {
        App.INSTANCE.quizSettingsDataStore.edit { preferences ->
            preferences[QuizSettingsKeys.QUIZ_MODE] = quizMode.preferenceName
        }
    }

}