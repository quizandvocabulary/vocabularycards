package me.asddsajpgnoob.quizandvocabulary.data.local.typeConverter

import androidx.room.TypeConverter
import me.asddsajpgnoob.quizandvocabulary.util.DateUtils
import java.util.*

class DateTypeConverter {

    @TypeConverter
    fun toDate(source: String): Date = DateUtils.stringToDate(
        source,
        pattern = DateUtils.DB_DATE_TIME_FORMAT,
        timeZone = TimeZone.getTimeZone("UTC"),
        locale = Locale.US
    ) ?: Date()

    @TypeConverter
    fun fromDate(date: Date): String = DateUtils.dateToString(
        date,
        pattern = DateUtils.DB_DATE_TIME_FORMAT,
        timeZone = TimeZone.getTimeZone("UTC"),
        locale = Locale.US
    )
}