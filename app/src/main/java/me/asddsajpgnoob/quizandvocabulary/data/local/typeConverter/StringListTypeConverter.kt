package me.asddsajpgnoob.quizandvocabulary.data.local.typeConverter

import androidx.room.TypeConverter
import com.google.gson.GsonBuilder

class StringListTypeConverter {

    private val gson = GsonBuilder().create()

    @TypeConverter
    fun toList(value: String): List<String> = gson.fromJson<List<String>>(value, List::class.java)

    @TypeConverter
    fun toJsonString(list: List<String>): String = gson.toJson(list)
}