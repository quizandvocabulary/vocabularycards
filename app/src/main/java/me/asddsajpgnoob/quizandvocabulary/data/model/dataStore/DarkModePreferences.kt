package me.asddsajpgnoob.quizandvocabulary.data.model.dataStore

enum class DarkModePreferences(val preferenceName: String) {
    DARK("dark"),
    LIGHT("light"),
    FOLLOW_SYSTEM("follow_system");

    companion object {
        fun findByPreferenceName(preferenceName: String): DarkModePreferences =
            enumValues<DarkModePreferences>().find {
                it.preferenceName == preferenceName
            } ?: throw RuntimeException("no such ${DarkModePreferences::class.simpleName} with name $preferenceName")
    }
}