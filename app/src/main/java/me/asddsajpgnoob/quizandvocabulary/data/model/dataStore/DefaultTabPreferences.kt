package me.asddsajpgnoob.quizandvocabulary.data.model.dataStore

enum class DefaultTabPreferences(val preferenceName: String) {
    QUIZ("quiz"),
    VOCABULARY("vocabulary");

    companion object {
        fun findByPreferenceName(preferenceName: String): DefaultTabPreferences =
            enumValues<DefaultTabPreferences>().find {
                it.preferenceName == preferenceName
            } ?: throw RuntimeException("no such ${DefaultTabPreferences::class.simpleName} with name $preferenceName")
    }
}