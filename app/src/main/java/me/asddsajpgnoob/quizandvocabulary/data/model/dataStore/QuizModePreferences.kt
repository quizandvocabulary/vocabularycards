package me.asddsajpgnoob.quizandvocabulary.data.model.dataStore

enum class QuizModePreferences(val preferenceName: String) {
    RANDOM("random"),
    FIRST_LESS_GUESSED("firstLessGuessed"),
    FIRST_LESS_GUESSED_WITH_RANDOM("firstLessGuessedWithRandom"),
    NEWEST_FIRST("newestFirst"),
    OLDEST_FIRST("oldestFirst");

    companion object {
        fun findByPreferenceName(preferenceName: String): QuizModePreferences =
            enumValues<QuizModePreferences>().find {
                it.preferenceName == preferenceName
            } ?: throw RuntimeException("no such ${QuizModePreferences::class.simpleName} with name $preferenceName")
    }
}