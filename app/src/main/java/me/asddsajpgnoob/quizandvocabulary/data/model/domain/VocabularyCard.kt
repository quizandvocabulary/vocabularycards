package me.asddsajpgnoob.quizandvocabulary.data.model.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import me.asddsajpgnoob.quizandvocabulary.data.model.room.VocabularyCardEntity
import java.util.*

@Parcelize
data class VocabularyCard(
    val origin: String,

    val translation: String,

    val examples: List<String>,

    val audioFiles: List<String>,

    val showInQuiz: Boolean,

    val guessedCount: Int,

    val notGuessedCount: Int,

    val createdAt: Date,

    val updatedAt: Date,

    val id: Long
): Parcelable {

    fun toEntity(): VocabularyCardEntity = VocabularyCardEntity(
        origin,
        translation,
        examples,
        audioFiles,
        showInQuiz,
        guessedCount,
        notGuessedCount,
        createdAt,
        updatedAt,
        id
    )
}