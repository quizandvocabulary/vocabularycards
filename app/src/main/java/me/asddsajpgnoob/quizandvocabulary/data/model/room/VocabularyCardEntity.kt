package me.asddsajpgnoob.quizandvocabulary.data.model.room

import androidx.room.*
import me.asddsajpgnoob.quizandvocabulary.data.local.typeConverter.DateTypeConverter
import me.asddsajpgnoob.quizandvocabulary.data.local.typeConverter.StringListTypeConverter
import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard
import java.util.*

@TypeConverters(
    value = [
        StringListTypeConverter::class,
        DateTypeConverter::class
    ]
)
@Entity(
    tableName = "vocabulary_cards",
    indices = [
        Index(value = ["origin"], unique = false),
        Index(value = ["translation"], unique = false)
    ]
)
data class VocabularyCardEntity(
    @ColumnInfo(name = "origin")
    val origin: String,

    @ColumnInfo(name = "translation")
    val translation: String,

    @ColumnInfo(name = "examples")
    val examples: List<String>,

    @ColumnInfo(name = "audio_files")
    val audioFiles: List<String>,

    @ColumnInfo(name = "show_in_quiz")
    val showInQuiz: Boolean,

    @ColumnInfo(name = "guessed_info")
    val guessedCount: Int,

    @ColumnInfo(name = "not_guessed_count")
    val notGuessedCount: Int,

    @ColumnInfo(name = "created_at")
    val createdAt: Date,

    @ColumnInfo(name = "updatedAt")
    val updatedAt: Date,

    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L
) {

    companion object {
        fun List<VocabularyCardEntity>.toDomainList(): List<VocabularyCard> = map {
            it.toDomain()
        }
    }

    fun toDomain(): VocabularyCard = VocabularyCard(
        origin,
        translation,
        examples,
        audioFiles,
        showInQuiz,
        guessedCount,
        notGuessedCount,
        createdAt,
        updatedAt,
        id
    )

}
