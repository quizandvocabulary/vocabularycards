package me.asddsajpgnoob.quizandvocabulary.data.repository

import kotlinx.coroutines.flow.Flow
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DefaultTabPreferences

interface InterfaceSettingsRepository {

    val darkMode: Flow<DarkModePreferences>

    val defaultTab: Flow<DefaultTabPreferences>

    val showAudiosInVocabularyScreen: Flow<Boolean>

    val showExamplesInVocabularyScreen: Flow<Boolean>

    val openTranslatorOnTopOfApp: Flow<Boolean>

    val saveNavigationStateWhenSwitchingTab: Flow<Boolean>

    suspend fun setDarkMode(darkMode: DarkModePreferences)

    suspend fun setDefaultTab(defaultTab: DefaultTabPreferences)

    suspend fun setShowAudiosInVocabularyScreen(show: Boolean)

    suspend fun setExamplesAudiosInVocabularyScreen(show: Boolean)

    suspend fun setOpenTranslatorOnTopOfApp(open: Boolean)

    suspend fun setSaveNavigationStateWhenSwitchingTab(save: Boolean)
}