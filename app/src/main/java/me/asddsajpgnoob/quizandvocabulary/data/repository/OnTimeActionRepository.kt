package me.asddsajpgnoob.quizandvocabulary.data.repository

import kotlinx.coroutines.flow.Flow

interface OnTimeActionRepository {

    val showQuizTouchHint: Flow<Boolean>

    val showQuizSwipeDirectionHint: Flow<Boolean>

    suspend fun setShowQuizTouchHint(show: Boolean)

    suspend fun setShowQuizSwipeDirectionHint(show: Boolean)
}