package me.asddsajpgnoob.quizandvocabulary.data.repository

import kotlinx.coroutines.flow.Flow
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.QuizModePreferences

interface QuizSettingsRepository {

    val quizMode: Flow<QuizModePreferences>

    suspend fun setQuizMode(quizMode: QuizModePreferences)
}