package me.asddsajpgnoob.quizandvocabulary.data.repository

import me.asddsajpgnoob.quizandvocabulary.data.model.room.VocabularyCardEntity

interface VocabularyCardRepository {

    suspend fun searchVocabularyCardsByOriginOrTranslationOrderByCreatedAtDesc(query: String, offset: Int, limit: Int): List<VocabularyCardEntity>

    suspend fun findRandomVocabularyCard(exceptionIds: List<Long>): VocabularyCardEntity?

    suspend fun findLessGuessedVocabularyCards(offset: Int, limit: Int): List<VocabularyCardEntity>

    suspend fun findNewestVocabularyCards(offset: Int, limit: Int): List<VocabularyCardEntity>

    suspend fun findOldestVocabularyCards(offset: Int, limit: Int): List<VocabularyCardEntity>

    suspend fun insert(entity: VocabularyCardEntity): Long

    suspend fun update(entity: VocabularyCardEntity)

    suspend fun delete(entity: VocabularyCardEntity)
}