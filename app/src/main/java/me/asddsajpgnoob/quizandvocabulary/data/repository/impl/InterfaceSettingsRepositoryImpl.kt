package me.asddsajpgnoob.quizandvocabulary.data.repository.impl

import kotlinx.coroutines.flow.Flow
import me.asddsajpgnoob.quizandvocabulary.data.local.preferences.InterfaceSettingsPreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabulary.data.repository.InterfaceSettingsRepository

object InterfaceSettingsRepositoryImpl : InterfaceSettingsRepository {

    override val darkMode: Flow<DarkModePreferences> = InterfaceSettingsPreferences.darkMode

    override val defaultTab: Flow<DefaultTabPreferences> = InterfaceSettingsPreferences.defaultTab

    override val showAudiosInVocabularyScreen: Flow<Boolean> = InterfaceSettingsPreferences.showAudiosInVocabularyScreen

    override val showExamplesInVocabularyScreen: Flow<Boolean> = InterfaceSettingsPreferences.showExamplesInVocabularyScreen

    override val openTranslatorOnTopOfApp: Flow<Boolean> = InterfaceSettingsPreferences.openTranslatorOnTopOfApp

    override val saveNavigationStateWhenSwitchingTab: Flow<Boolean> = InterfaceSettingsPreferences.saveNavigationStateWhenSwitchingTab

    override suspend fun setDarkMode(darkMode: DarkModePreferences) {
        InterfaceSettingsPreferences.setDarkMode(darkMode)
    }

    override suspend fun setDefaultTab(defaultTab: DefaultTabPreferences) {
        InterfaceSettingsPreferences.setDefaultTab(defaultTab)
    }

    override suspend fun setShowAudiosInVocabularyScreen(show: Boolean) {
        InterfaceSettingsPreferences.setShowAudiosInVocabularyScreen(show)
    }

    override suspend fun setExamplesAudiosInVocabularyScreen(show: Boolean) {
        InterfaceSettingsPreferences.setExamplesAudiosInVocabularyScreen(show)
    }

    override suspend fun setOpenTranslatorOnTopOfApp(open: Boolean) {
        InterfaceSettingsPreferences.setOpenTranslatorOnTopOfApp(open)
    }

    override suspend fun setSaveNavigationStateWhenSwitchingTab(save: Boolean) {
        InterfaceSettingsPreferences.setSaveNavigationStateWhenSwitchingTab(save)
    }

}

