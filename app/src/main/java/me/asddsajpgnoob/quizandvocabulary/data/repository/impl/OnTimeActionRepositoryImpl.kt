package me.asddsajpgnoob.quizandvocabulary.data.repository.impl

import me.asddsajpgnoob.quizandvocabulary.data.local.preferences.OnTimeActionPreferences
import me.asddsajpgnoob.quizandvocabulary.data.repository.OnTimeActionRepository

object OnTimeActionRepositoryImpl : OnTimeActionRepository {

    override val showQuizTouchHint = OnTimeActionPreferences.showQuizTouchHint

    override val showQuizSwipeDirectionHint = OnTimeActionPreferences.showQuizSwipeDirectionHint

    override suspend fun setShowQuizTouchHint(show: Boolean) {
        OnTimeActionPreferences.setShowQuizTouchHint(show)
    }

    override suspend fun setShowQuizSwipeDirectionHint(show: Boolean) {
        OnTimeActionPreferences.setShowQuizSwipeDirectionHint(show)
    }
}