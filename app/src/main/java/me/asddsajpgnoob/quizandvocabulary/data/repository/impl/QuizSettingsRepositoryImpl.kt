package me.asddsajpgnoob.quizandvocabulary.data.repository.impl

import kotlinx.coroutines.flow.Flow
import me.asddsajpgnoob.quizandvocabulary.data.local.preferences.QuizSettingsPreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.QuizModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.repository.QuizSettingsRepository

object QuizSettingsRepositoryImpl : QuizSettingsRepository{

    override val quizMode = QuizSettingsPreferences.quizMode

    override suspend fun setQuizMode(quizMode: QuizModePreferences) {
        QuizSettingsPreferences.setQuizMode(quizMode)
    }
}