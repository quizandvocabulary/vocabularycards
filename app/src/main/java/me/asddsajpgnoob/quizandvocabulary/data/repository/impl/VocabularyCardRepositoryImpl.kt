package me.asddsajpgnoob.quizandvocabulary.data.repository.impl

import me.asddsajpgnoob.quizandvocabulary.data.local.VocabularyCardsDatabase
import me.asddsajpgnoob.quizandvocabulary.data.model.room.VocabularyCardEntity
import me.asddsajpgnoob.quizandvocabulary.data.repository.VocabularyCardRepository

object VocabularyCardRepositoryImpl : VocabularyCardRepository {

    private val db = VocabularyCardsDatabase.get()

    private val dao = db.vocabularyCardDao()

    override suspend fun searchVocabularyCardsByOriginOrTranslationOrderByCreatedAtDesc(
        query: String,
        offset: Int,
        limit: Int
    ) = dao.searchByOriginOrTranslationOrderByCreatedAtDesc(query, offset, limit)

    override suspend fun findRandomVocabularyCard(exceptionIds: List<Long>) =
        dao.findRandomWithExceptions(exceptionIds)

    override suspend fun findLessGuessedVocabularyCards(
        offset: Int,
        limit: Int
    ) = dao.findAllOrderByLessGuessedDesc(offset, limit)

    override suspend fun findNewestVocabularyCards(offset: Int, limit: Int) = dao.findAllOrderByCreatedAtDesc(offset, limit)

    override suspend fun findOldestVocabularyCards(offset: Int, limit: Int) = dao.findAllOrderByCreatedAtAsc(offset, limit)

    override suspend fun insert(entity: VocabularyCardEntity) = dao.insert(entity)

    override suspend fun update(entity: VocabularyCardEntity) = dao.update(entity)

    override suspend fun delete(entity: VocabularyCardEntity) = dao.delete(entity)
}