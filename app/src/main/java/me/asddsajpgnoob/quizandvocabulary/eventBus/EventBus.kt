package me.asddsajpgnoob.quizandvocabulary.eventBus

import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import me.asddsajpgnoob.quizandvocabulary.eventBus.model.VocabularyCardEvent

object EventBus {

    private val _vocabularyCardEvent = MutableSharedFlow<VocabularyCardEvent>()
    val vocabularyCardEvent = _vocabularyCardEvent.asSharedFlow()

    suspend fun submitVocabularyCardEvent(event: VocabularyCardEvent) {
        _vocabularyCardEvent.emit(event)
    }
}