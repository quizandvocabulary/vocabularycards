package me.asddsajpgnoob.quizandvocabulary.eventBus.model

import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard

sealed interface VocabularyCardEvent {

    data class CardCreated(val card: VocabularyCard) : VocabularyCardEvent

    data class CardDeleted(val card: VocabularyCard) : VocabularyCardEvent

    data class CardEdited(val oldCard: VocabularyCard, val editedCard: VocabularyCard) : VocabularyCardEvent

}