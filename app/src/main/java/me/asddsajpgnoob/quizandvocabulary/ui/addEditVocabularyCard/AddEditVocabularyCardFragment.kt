package me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.*
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import kotlinx.coroutines.flow.collectLatest
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.VocabularyCardRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.databinding.FragmentAddEditVocabularyCardBinding
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.adapter.EditableExamplesWithFooterAdapter
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.adapter.RemovableAudiosWithFooterAdapter
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.model.AddEditVocabularyEvent
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.model.AudioListItem
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.model.ExampleListItem
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog.RecordAudioDialogFragment
import me.asddsajpgnoob.quizandvocabulary.ui.main.MainViewModel
import me.asddsajpgnoob.quizandvocabulary.util.Constants
import me.asddsajpgnoob.quizandvocabulary.util.IntentUtils
import me.asddsajpgnoob.quizandvocabulary.util.applyNewTaskIf
import java.io.File

class AddEditVocabularyCardFragment : BaseFragment<FragmentAddEditVocabularyCardBinding>(),
    EditableExamplesWithFooterAdapter.ExamplesAdapterListener,
    RemovableAudiosWithFooterAdapter.AudiosAdapterListener {

    companion object {
        const val CURRENT_DESTINATION_ID = R.id.addEditVocabularyCardFragment
    }

    private val nevArgs: AddEditVocabularyCardFragmentArgs by navArgs()

    private val examplesAdapter = EditableExamplesWithFooterAdapter(this)

    private val audiosAdapter = RemovableAudiosWithFooterAdapter(this)

    private val viewModel: AddEditVocabularyCardViewModel by viewModels {
        AddEditVocabularyCardViewModel.Factory(
            this,
            nevArgs.toBundle(),
            InterfaceSettingsRepositoryImpl,
            VocabularyCardRepositoryImpl,
            requireContext().getExternalFilesDir(null)!!,
        )
    }

    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentAddEditVocabularyCardBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        setFragmentResultListeners()

        setupView()
        setupRecyclerViews()
        setOnTextChangeListeners()
        setOnClickListeners()
        setOnCheckedChangedListener()

        collectShowInQuiz()
        collectExamples()
        collectPlaybackInfo()
        collectAudioFiles()
        collectSubmitButtonEnabled()

        collectEvents()

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_edit_vocabulary_card, menu)
        if (viewModel.initialVocabularyCard == null) {
            val itemDeleteCard = menu.findItem(R.id.item_delete_card)
            itemDeleteCard.isVisible = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_delete_card -> {
                viewModel.onMenuItemDeleteCardSelected()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun setFragmentResultListeners() {
        parentFragmentManager.setFragmentResultListener(
            RecordAudioDialogFragment.REQUEST_KEY_RECORD_AUDIO,
            this
        ) { _, bundle ->
            viewModel.onAudioRecorded(bundle.getSerializable(RecordAudioDialogFragment.KEY_AUDIO_FILE) as File)
        }
    }

    private fun setupView() {
        @StringRes val submitStringRes = if (viewModel.initialVocabularyCard == null) {
            R.string.Add
        } else {
            R.string.Save
        }
        binding.apply {
            buttonSubmit.setText(submitStringRes)

            editTextOrigin.setText(viewModel.textOrigin.value)
            editTextTranslation.setText(viewModel.textTranslation.value)

            if (viewModel.initialVocabularyCard != null) {
                textGuessedCount.text = getString(
                    R.string.Guessed_count_colon_number,
                    viewModel.initialVocabularyCard!!.guessedCount
                )
                textGuessedCount.isVisible = true

                textNotGuessedCount.text = getString(
                    R.string.Not_guessed_count_colon_number,
                    viewModel.initialVocabularyCard!!.notGuessedCount
                )
                textNotGuessedCount.isVisible = true
            } else {
                textGuessedCount.isVisible = false
                textNotGuessedCount.isVisible = false
            }
        }
    }

    private fun setupRecyclerViews() {
        binding.apply {
            recyclerViewExamples.setHasFixedSize(false)
            recyclerViewExamples.adapter = examplesAdapter

            recyclerViewAudios.setHasFixedSize(false)
            recyclerViewAudios.adapter = audiosAdapter
        }
    }

    private fun setOnTextChangeListeners() {
        binding.apply {
            editTextOrigin.doAfterTextChanged {
                viewModel.onEditTextOriginTextChanges(it)
            }
            editTextTranslation.doAfterTextChanged {
                viewModel.onEditTextTranslationChanges(it)
            }
        }
    }

    private fun setOnClickListeners() {
        binding.apply {
            buttonSubmit.setOnClickListener {
                viewModel.onButtonSubmitClicked()
            }
            imageButtonSearchOrigin.setOnClickListener {
                viewModel.onImageButtonSearchOriginClicked()
            }
        }
    }

    private fun setOnCheckedChangedListener() {
        binding.apply {
            checkBoxShowInQuiz.setOnCheckedChangeListener { _, isChecked ->
                viewModel.onCheckboxShowInQuizCheckedChange(isChecked)
            }
        }
    }

    private fun collectShowInQuiz() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.showInQuiz.collectLatest {
                binding.checkBoxShowInQuiz.isChecked = it
            }
        }
    }

    private fun collectExamples() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.examplesListItems.collectLatest {
                examplesAdapter.submitListData(it)
            }
        }
    }

    private fun collectPlaybackInfo() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.playbackInfo.collectLatest {
                audiosAdapter.updatePlaybackInfo(it)
            }
        }
    }

    private fun collectAudioFiles() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.audioListItems.collectLatest {
                audiosAdapter.submitListData(viewModel.playbackInfo.value, it)
            }
        }
    }

    private fun collectSubmitButtonEnabled() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.submitButtonEnabled.collectLatest {
                binding.buttonSubmit.isEnabled = it
            }
        }
    }

    private fun collectEvents() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.events.collect { event ->
                when (event) {
                    AddEditVocabularyEvent.NavigateToRecordAudioDialog -> {
                        val action =
                            AddEditVocabularyCardFragmentDirections.actionAddEditVocabularyCardFragmentToRecordAudioDialogFragment()
                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                    }
                    is AddEditVocabularyEvent.NavigateToGoogleTranslate -> {
                        val translateBrowserIntent = IntentUtils.getActionViewIntent(
                            String.format(
                                Constants.GOOGLE_TRANSLATE_URL_UNFORMATTED,
                                event.origin
                            )
                        ).applyNewTaskIf(!event.openOnTopOfApp)

                        val translateAppIntent = IntentUtils.getActionViewIntent(
                            Constants.GOOGLE_TRANSLATE_APP_URL
                        ).applyNewTaskIf(!event.openOnTopOfApp)

                        val chooser = IntentUtils.createChooser(
                            translateBrowserIntent,
                            arrayOf(translateAppIntent),
                            getString(R.string.Open_with_dots)
                        )
                        startActivity(chooser)
                    }
                    AddEditVocabularyEvent.PopBackStack -> {
                        popBackStack()
                    }
                    is AddEditVocabularyEvent.ShowToast -> {
                        showToast(event.stringRes)
                    }
                }
            }
        }
    }

    override fun onDeleteExampleClick(item: ExampleListItem.Example) {
        viewModel.onDeleteExampleClicked(item)
    }

    override fun onExamplesEdited(position: Int, newValue: String) {
        viewModel.onExamplesEdited(position, newValue)
    }

    override fun onAddExampleClicked() {
        viewModel.onAddExampleClicked()
    }

    override fun onPlayPauseClicked(audioItem: AudioListItem.Audio) {
        viewModel.onPlayPauseClicked(audioItem)
    }

    override fun onDeleteAudioClicked(item: AudioListItem.Audio) {
        viewModel.onDeleteAudioClicked(item)
    }

    override fun onAddAudioClicked() {
        viewModel.onAddAudioClicked()
    }

}