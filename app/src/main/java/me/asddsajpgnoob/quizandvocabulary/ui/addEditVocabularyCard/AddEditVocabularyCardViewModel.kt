package me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard

import android.media.MediaPlayer
import android.os.Bundle
import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.savedstate.SavedStateRegistryOwner
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabulary.data.repository.InterfaceSettingsRepository
import me.asddsajpgnoob.quizandvocabulary.data.repository.VocabularyCardRepository
import me.asddsajpgnoob.quizandvocabulary.eventBus.EventBus
import me.asddsajpgnoob.quizandvocabulary.eventBus.model.VocabularyCardEvent
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.model.AddEditVocabularyEvent
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.model.AudioListItem
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.model.ExampleListItem
import me.asddsajpgnoob.quizandvocabulary.ui.common.model.PlaybackInfo
import me.asddsajpgnoob.quizandvocabulary.util.AudioPlayerFactory
import me.asddsajpgnoob.quizandvocabulary.util.Constants
import java.io.File
import java.util.*

class AddEditVocabularyCardViewModel(
    savedStateHandle: SavedStateHandle,
    interfaceSettingsRepository: InterfaceSettingsRepository,
    private val vocabularyCardRepository: VocabularyCardRepository,
    private val externalFilesDir: File
) : ViewModel() {

    companion object {
        private const val KEY_CARD = "card"
    }

    val initialVocabularyCard: VocabularyCard? = savedStateHandle.get(KEY_CARD)

    private var finalVocabularyCard = initialVocabularyCard

    private val _textOrigin = MutableStateFlow(initialVocabularyCard?.origin)
    val textOrigin = _textOrigin.asStateFlow()

    private val _textTranslation = MutableStateFlow(initialVocabularyCard?.translation)
    val textTranslation = _textTranslation.asStateFlow()

    private val openTranslatorOnTopOfApp = interfaceSettingsRepository.openTranslatorOnTopOfApp

    private val _showInQuiz =
        MutableStateFlow(initialVocabularyCard?.showInQuiz ?: Constants.ADD_EDIT_CARD_SHOW_IN_QUIZ_INITIAL_VALUE)
    val showInQuiz = _showInQuiz.asStateFlow()

    private val _examples = MutableStateFlow<List<String>>(
        mutableListOf<String>().apply {
            initialVocabularyCard?.let {
                addAll(it.examples)
            }
        }
    )
    val examplesListItems: Flow<List<ExampleListItem>> = _examples.map { examples ->
        val result = mutableListOf<ExampleListItem>()
        for (i in examples.indices) {
            result.add(ExampleListItem.Example(examples[i]))
        }
        result.add(ExampleListItem.Footer)
        result
    }

    private val _audioFiles = MutableStateFlow<List<String>>(
        mutableListOf<String>().apply {
            initialVocabularyCard?.let {
                addAll(it.audioFiles)
            }
        }
    )

    val audioListItems: Flow<List<AudioListItem>> = _audioFiles.map { audioFiles ->
        val result = mutableListOf<AudioListItem>()
        for (i in audioFiles.indices) {
            result.add(AudioListItem.Audio(audioFiles[i]))
        }
        result.add(AudioListItem.Footer)
        result
    }

    val submitButtonEnabled = combine(
        _textOrigin,
        _textTranslation,
        _showInQuiz,
        _examples,
        _audioFiles
    ) { textOrigin, textTranslations, showInQuizValue, examplesList, audioFilesList ->
        finalVocabularyCard = VocabularyCard(
            textOrigin ?: "",
            textTranslations ?: "",
            examplesList.filter {
                it.isNotBlank()
            },
            audioFilesList,
            showInQuizValue,
            initialVocabularyCard?.guessedCount ?: 0,
            initialVocabularyCard?.notGuessedCount ?: 0,
            initialVocabularyCard?.createdAt ?: Date(),
            Date(),
            initialVocabularyCard?.id ?: 0L
        )
        finalVocabularyCard!!.run {
            origin.isNotBlank() &&
                    translation.isNotBlank() &&
                    (origin != initialVocabularyCard?.origin ?: "" ||
                            translation != initialVocabularyCard?.translation ?: "" ||
                            showInQuiz != initialVocabularyCard?.showInQuiz ?: Constants.ADD_EDIT_CARD_SHOW_IN_QUIZ_INITIAL_VALUE ||
                            examples != initialVocabularyCard?.examples ?: listOf<String>() ||
                            audioFiles != initialVocabularyCard?.audioFiles ?: listOf<String>()
                            )

        }
    }

    private val _playbackInfo = MutableStateFlow<PlaybackInfo?>(null)
    val playbackInfo = _playbackInfo.asStateFlow()

    private val eventsChannel = Channel<AddEditVocabularyEvent>()
    val events = eventsChannel.receiveAsFlow()

    private var mediaPlayerPositionJob: Job? = null
    private var mediaPlayer: MediaPlayer? = null

    private val removedAudioFileNames = mutableListOf<String>()

    private var cardDeleted = false
    private var submitted = false

    fun onDeleteExampleClicked(example: ExampleListItem.Example) {
        val result = _examples.value.toMutableList()
        result.remove(example.example)
        _examples.value = result
    }

    fun onExamplesEdited(position: Int, newValue: String) {
        val result = _examples.value.toMutableList()
        result[position] = newValue
        _examples.value = result
    }

    fun onDeleteAudioClicked(audio: AudioListItem.Audio) {
        val playingInfo = _playbackInfo.value
        if (playingInfo != null && playingInfo.playing && playingInfo.fileName == audio.fileName) {
            releaseMediaPlayer()
        }
        removedAudioFileNames.add(audio.fileName)
        val result = _audioFiles.value.toMutableList()
        result.remove(audio.fileName)
        _audioFiles.value = result
    }

    fun onEditTextOriginTextChanges(origin: Editable?) {
        _textOrigin.value = origin?.toString()
    }

    fun onEditTextTranslationChanges(translation: Editable?) {
        _textTranslation.value = translation?.toString()
    }

    fun onCheckboxShowInQuizCheckedChange(checked: Boolean) {
        _showInQuiz.value = checked
    }

    fun onButtonSubmitClicked() = viewModelScope.launch {
        submitted = true
        if (initialVocabularyCard == null) {
            val id = vocabularyCardRepository.insert(finalVocabularyCard!!.toEntity())
            finalVocabularyCard = finalVocabularyCard!!.copy(
                id = id
            )
            EventBus.submitVocabularyCardEvent(VocabularyCardEvent.CardCreated(finalVocabularyCard!!))
        } else {
            vocabularyCardRepository.update(finalVocabularyCard!!.toEntity())
            EventBus.submitVocabularyCardEvent(VocabularyCardEvent.CardEdited(initialVocabularyCard, finalVocabularyCard!!))
        }
        eventsChannel.send(AddEditVocabularyEvent.PopBackStack)
    }

    fun onImageButtonSearchOriginClicked() = viewModelScope.launch {
        _textOrigin.value.let {
            if (!it.isNullOrBlank()) {
                eventsChannel.send(AddEditVocabularyEvent.NavigateToGoogleTranslate(it, openTranslatorOnTopOfApp.first()))
            }
        }
    }

    fun onMenuItemDeleteCardSelected() = viewModelScope.launch {
        vocabularyCardRepository.delete(initialVocabularyCard!!.toEntity())
        cardDeleted = true
        EventBus.submitVocabularyCardEvent(VocabularyCardEvent.CardDeleted(initialVocabularyCard))
        eventsChannel.send(AddEditVocabularyEvent.PopBackStack)
    }

    fun onAddAudioClicked() = viewModelScope.launch {
        eventsChannel.send(AddEditVocabularyEvent.NavigateToRecordAudioDialog)
    }

    fun onAudioRecorded(audioFile: File) {
        val result = _audioFiles.value.toMutableList()
        result.add(audioFile.name)
        _audioFiles.value = result
    }

    fun onAddExampleClicked() {
        val result = _examples.value.toMutableList()
        result.add("")
        _examples.value = result
    }

    private fun isMediaPlayerPlaying() = mediaPlayer?.isPlaying == true

    fun onPlayPauseClicked(item: AudioListItem.Audio) {
        val playingInfo = _playbackInfo.value
        if (playingInfo != null && playingInfo.fileName != item.fileName) {
            releaseMediaPlayer()
        }
        if (!isMediaPlayerPlaying()) {
            if (mediaPlayer == null) {
                val audioFile = File("${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}/${item.fileName}")
                if (!audioFile.exists()) {
                    showToast(R.string.Audio_file_not_found)
                    return
                }
                initMediaPlayer(audioFile)
            }
            startMediaPlayer()
            return
        }
        pauseMediaPlayer()
    }

    private fun showToast(@StringRes stringRes: Int) = viewModelScope.launch {
        eventsChannel.send(AddEditVocabularyEvent.ShowToast(stringRes))
    }

    private fun initMediaPlayer(audioFile: File) {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer = AudioPlayerFactory.newInstance(audioFile)
        mediaPlayer!!.prepare()
        _playbackInfo.value = PlaybackInfo(
            false,
            audioFile.name,
            mediaPlayer!!.duration,
            0
        )
        mediaPlayer!!.setOnCompletionListener {
            releaseMediaPlayer()
            _playbackInfo.value = null
        }
    }

    private fun startMediaPlayer() {
        mediaPlayerPositionJob = viewModelScope.launch {
            while (isActive) {
                _playbackInfo.value = _playbackInfo.value!!.copy(
                    playing = true,
                    playingPosition = mediaPlayer!!.currentPosition
                )
                delay(20L)
            }
        }
        mediaPlayer!!.start()
    }

    private fun pauseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer!!.pause()
        _playbackInfo.value = _playbackInfo.value!!.copy(
            playing = false
        )
    }

    private fun releaseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        if (isMediaPlayerPlaying()) {
            mediaPlayer!!.stop()
        }
        mediaPlayer?.release()
        mediaPlayer = null
        _playbackInfo.value = null
    }

    private fun deleteRemovedAudioFiles() {
        val audiosDirectoryPath =
            "${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}"
        removedAudioFileNames.forEach { fileName ->
            val file = File("${audiosDirectoryPath}/$fileName")
            file.delete()
        }
    }

    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()

        val audiosDirectoryPath =
            "${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}"

        if (cardDeleted) {
            deleteRemovedAudioFiles()
            if (finalVocabularyCard != null) {
                finalVocabularyCard!!.audioFiles.forEach { fileName ->
                    val audioFile = File("${audiosDirectoryPath}/$fileName")
                    audioFile.delete()
                }
            } else {
                initialVocabularyCard!!.audioFiles.forEach { fileName ->
                    val audioFile = File("${audiosDirectoryPath}/$fileName")
                    audioFile.delete()
                }
            }
            return
        }

        if (submitted) {
            deleteRemovedAudioFiles()
        } else {
            finalVocabularyCard?.let { finalCard ->
                if (initialVocabularyCard == null) {
                    deleteRemovedAudioFiles()
                    finalCard.audioFiles.forEach { fileName ->
                        val audioFile = File("${audiosDirectoryPath}/$fileName")
                        audioFile.delete()
                    }
                } else {
                    removedAudioFileNames.forEach { fileName ->
                        if (initialVocabularyCard.audioFiles.contains(fileName)) {
                            return@forEach
                        }
                        val audioFile = File("${audiosDirectoryPath}/$fileName")
                        audioFile.delete()
                    }
                    finalCard.audioFiles.forEach { fileName ->
                        if (initialVocabularyCard.audioFiles.contains(fileName)) {
                            return@forEach
                        }
                        val audioFile = File("${audiosDirectoryPath}/$fileName")
                        audioFile.delete()
                    }
                }
            }
        }
    }

    class Factory(
        owner: SavedStateRegistryOwner,
        navArgsBundle: Bundle?,
        private val interfaceSettingsRepository: InterfaceSettingsRepository,
        private val vocabularyCardRepository: VocabularyCardRepository,
        private val externalFilesDir: File
    ) : AbstractSavedStateViewModelFactory(owner, navArgsBundle) {

        override fun <T : ViewModel> create(
            key: String,
            modelClass: Class<T>,
            handle: SavedStateHandle
        ): T {
            return if (modelClass.isAssignableFrom(AddEditVocabularyCardViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                AddEditVocabularyCardViewModel(
                    handle,
                    interfaceSettingsRepository,
                    vocabularyCardRepository,
                    externalFilesDir
                ) as T
            } else {
                throw IllegalArgumentException("Unable to create ${AddEditVocabularyCardViewModel::class.qualifiedName}")
            }
        }
    }

}