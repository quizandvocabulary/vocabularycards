package me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.databinding.ItemButtonFooterBinding
import me.asddsajpgnoob.quizandvocabulary.databinding.ItemEditExampleBinding
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.model.ExampleListItem
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseViewHolder

class EditableExamplesWithFooterAdapter(
    private val listener: ExamplesAdapterListener
) : RecyclerView.Adapter<BaseViewHolder<ExampleListItem, *>>() {

    companion object {
        private const val ITEM_TYPE_EXAMPLE = 0
        private const val ITEM_TYPE_FOOTER = 1
    }

    private val items = mutableListOf<ExampleListItem>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<ExampleListItem, *> {
        return when (viewType) {
            ITEM_TYPE_EXAMPLE -> {
                val binding =
                    ItemEditExampleBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                ExampleViewHolder(binding)
            }
            ITEM_TYPE_FOOTER -> {
                val binding = ItemButtonFooterBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                FooterViewHolder(binding)
            }
            else -> {
                throw RuntimeException("no such viewType")
            }
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ExampleListItem, *>, position: Int) =
        holder.bind(items[position])

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is ExampleListItem.Example -> {
                ITEM_TYPE_EXAMPLE
            }
            ExampleListItem.Footer -> {
                ITEM_TYPE_FOOTER
            }
        }
    }

    fun submitListData(newList: List<ExampleListItem>) {
        val oldList = items.toList()
        items.clear()
        items.addAll(newList)
        val diffCallback = ExamplesDiffCallback(oldList, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback, false)
        diffResult.dispatchUpdatesTo(this)
    }

    inner class ExampleViewHolder(binding: ItemEditExampleBinding) :
        BaseViewHolder<ExampleListItem, ItemEditExampleBinding>(binding) {

        init {
            binding.apply {
                imageDeleteExample.setOnClickListener {
                    val pos = adapterPosition
                    if (pos != NO_POSITION) {
                        val exampleListItem = items[pos] as ExampleListItem.Example
                        listener.onDeleteExampleClick(exampleListItem)
                    }
                }
                editTextExample.doAfterTextChanged { editable ->
                    val pos = adapterPosition
                    if (pos != NO_POSITION) {
                        val newValue = editable?.toString() ?: ""
                        items[pos] = ExampleListItem.Example(newValue)
                        listener.onExamplesEdited(pos, newValue)
                    }
                }
            }

        }

        override fun bind(item: ExampleListItem) {
            val exampleItem = item as ExampleListItem.Example
            binding.editTextExample.setText(exampleItem.example)
        }
    }

    inner class FooterViewHolder(binding: ItemButtonFooterBinding) :
        BaseViewHolder<ExampleListItem, ItemButtonFooterBinding>(binding) {

        init {
            binding.buttonFooter.setOnClickListener {
                listener.onAddExampleClicked()
            }
        }

        override fun bind(item: ExampleListItem) {
            binding.buttonFooter.setText(R.string.Add_Example)
        }
    }

    class ExamplesDiffCallback(
        private val oldList: List<ExampleListItem>,
        private val newList: List<ExampleListItem>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition] == newList[newItemPosition]

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = true
    }

    interface ExamplesAdapterListener {
        fun onDeleteExampleClick(item: ExampleListItem.Example)

        fun onExamplesEdited(position: Int, newValue: String)

        fun onAddExampleClicked()
    }
}