package me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.databinding.ItemButtonFooterBinding
import me.asddsajpgnoob.quizandvocabulary.databinding.ItemEditAudioBinding
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.model.AudioListItem
import me.asddsajpgnoob.quizandvocabulary.ui.common.model.PlaybackInfo
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseViewHolder

class RemovableAudiosWithFooterAdapter(
    private val listener: AudiosAdapterListener
) : RecyclerView.Adapter<BaseViewHolder<AudioListItem, *>>() {

    companion object {
        private const val ITEM_TYPE_AUDIO = 0
        private const val ITEM_TYPE_FOOTER = 1
    }

    private var currentPlaybackInfo: PlaybackInfo? = null
    private var playingViewHolder: AudioViewHolder? = null

    private var items = mutableListOf<AudioListItem>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<AudioListItem, *> {
        return when (viewType) {
            ITEM_TYPE_AUDIO -> {
                val binding =
                    ItemEditAudioBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                AudioViewHolder(binding)
            }
            ITEM_TYPE_FOOTER -> {
                val binding = ItemButtonFooterBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                FooterViewHolder(binding)
            }
            else -> {
                throw RuntimeException("no such viewType")
            }
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<AudioListItem, *>, position: Int) =
        holder.bind(items[position])

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is AudioListItem.Audio -> {
                ITEM_TYPE_AUDIO
            }
            AudioListItem.Footer -> {
                ITEM_TYPE_FOOTER
            }
        }
    }

    override fun onViewRecycled(holder: BaseViewHolder<AudioListItem, *>) {
        super.onViewRecycled(holder)
        val pos = holder.adapterPosition
        if (pos != NO_POSITION) {
            if (holder is AudioViewHolder) {
                val item = items[holder.adapterPosition] as AudioListItem.Audio
                val playingInfo = currentPlaybackInfo
                if (item.fileName == playingInfo?.fileName) {
                    playingViewHolder?.updatePlaying(false, playingInfo.duration, 0)
                    playingViewHolder = null
                }
            }
        }
    }

    fun submitListData(playingInfo: PlaybackInfo?, newItems: List<AudioListItem>) {
        val oldItems = items.toList()
        items.clear()
        items.addAll(newItems)
        val diffCallback = AudioDiffCallback(oldItems, items, playingInfo)
        val diffResult = DiffUtil.calculateDiff(diffCallback, false)
        diffResult.dispatchUpdatesTo(this)
    }

    fun updatePlaybackInfo(newPlaybackInfo: PlaybackInfo?) {
        if (newPlaybackInfo == null) {
            playingViewHolder?.updatePlaying(false, currentPlaybackInfo?.duration ?: 100, 0)
        } else {
            playingViewHolder?.updatePlaying(newPlaybackInfo.playing, newPlaybackInfo.duration, newPlaybackInfo.playingPosition)
        }
        currentPlaybackInfo = newPlaybackInfo
    }

    inner class AudioViewHolder(binding: ItemEditAudioBinding) :
        BaseViewHolder<AudioListItem, ItemEditAudioBinding>(binding) {

        init {
            binding.apply {
                audioSeekBar.isEnabled = false
                imagePlayPause.setOnClickListener {
                    val pos = adapterPosition
                    if (pos != NO_POSITION) {
                        playingViewHolder?.updatePlaying(false, 100, 0)
                        playingViewHolder = this@AudioViewHolder

                        val audioListItem = items[pos] as AudioListItem.Audio
                        listener.onPlayPauseClicked(audioListItem)
                    }
                }
                imageDeleteExample.setOnClickListener {
                    val pos = adapterPosition
                    if (pos != NO_POSITION) {
                        val audioListItem = items[pos] as AudioListItem.Audio
                        listener.onDeleteAudioClicked(audioListItem)
                    }
                }
            }
        }

        fun updatePlaying(playing: Boolean, duration: Int, progress: Int) {
            @DrawableRes val playPauseIconRes: Int = if (playing) {
                R.drawable.ic_pause_circle_filled_orange_24
            } else {
                R.drawable.ic_play_circle_filled_orange_24
            }
            binding.apply {
                imagePlayPause.setImageResource(playPauseIconRes)
                audioSeekBar.max = duration
                audioSeekBar.progress = progress
            }
        }

        override fun bind(item: AudioListItem) {
            val audioListItem = item as AudioListItem.Audio
            val playingInfo = currentPlaybackInfo
            if (playingInfo == null) {
                updatePlaying(false, 100, 0)
                return
            }
            val playing = playingInfo.fileName == audioListItem.fileName
            if (playing) {
                playingViewHolder = this
                updatePlaying(playing, playingInfo.duration, playingInfo.playingPosition)
            } else {
                updatePlaying(playing, 100, 0)
            }
        }
    }

    inner class FooterViewHolder(binding: ItemButtonFooterBinding) :
        BaseViewHolder<AudioListItem, ItemButtonFooterBinding>(binding) {

        init {
            binding.buttonFooter.setOnClickListener {
                listener.onAddAudioClicked()
            }
        }

        override fun bind(item: AudioListItem) {
            binding.buttonFooter.setText(R.string.Add_Audio)
        }
    }

    class AudioDiffCallback(
        private val oldList: List<AudioListItem>,
        private val newList: List<AudioListItem>,
        private val playingInfo: PlaybackInfo?
    ) : DiffUtil.Callback() {
        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldList[oldItemPosition]
            val newItem = newList[newItemPosition]

            if (oldItem::class != newItem::class) {
                return false
            }

            if (oldItem is AudioListItem.Footer) {
                return true
            }

            return (oldItem as AudioListItem.Audio).fileName == (newItem as AudioListItem.Audio).fileName
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldList[oldItemPosition]
            val newItem = newList[newItemPosition]

            if (oldItem is AudioListItem.Footer) {
                return true
            }

            playingInfo?.let {
                if ((oldItem as AudioListItem.Audio).fileName == it.fileName || (newItem as AudioListItem.Audio).fileName == it.fileName) {
                    return false
                }
            }

            return true
        }

    }

    interface AudiosAdapterListener {
        fun onPlayPauseClicked(audioItem: AudioListItem.Audio)

        fun onDeleteAudioClicked(item: AudioListItem.Audio)

        fun onAddAudioClicked()
    }
}