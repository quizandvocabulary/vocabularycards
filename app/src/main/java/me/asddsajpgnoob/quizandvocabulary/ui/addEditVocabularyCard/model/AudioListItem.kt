package me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.model

sealed interface AudioListItem {

    data class Audio(val fileName: String) : AudioListItem

    object Footer : AudioListItem

}