package me.asddsajpgnoob.quizandvocabulary.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import me.asddsajpgnoob.quizandvocabulary.ui.main.MainViewModel

abstract class BaseFragment<VB : ViewBinding> : Fragment() {

    private var _binding: VB? = null
    protected val binding get() = _binding!!

    protected abstract val mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = getViewBinding()

        mainViewModel.setOrientation(getOrientation())

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    abstract fun getViewBinding(): VB

    abstract fun getOrientation(): Int

    /**
     * @return true if navigated false otherwise
     * */
    protected fun navigateIfInDestination(
        @IdRes destinationId: Int,
        directions: NavDirections
    ): Boolean {
        val navController = findNavController()
        if (navController.currentDestination?.id == destinationId) {
            navController.navigate(directions)
            return true
        }
        return false
    }

    protected fun navigate(directions: NavDirections) {
        findNavController().navigate(directions)
    }

    protected fun popBackStack() {
        findNavController().popBackStack()
    }

    protected fun showToast(toast: String) {
        Toast.makeText(requireContext(), toast, Toast.LENGTH_SHORT).show()
    }

    protected fun showToast(@StringRes stringRes: Int) {
        Toast.makeText(requireContext(), stringRes, Toast.LENGTH_SHORT).show()
    }

    protected fun showLongToast(toast: String) {
        Toast.makeText(requireContext(), toast, Toast.LENGTH_LONG).show()
    }

    protected fun showLongToast(@StringRes stringRes: Int) {
        Toast.makeText(requireContext(), stringRes, Toast.LENGTH_LONG).show()
    }
}