package me.asddsajpgnoob.quizandvocabulary.ui.dialog.genericAlertDialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import me.asddsajpgnoob.quizandvocabulary.R

class GenericAlertDialog : DialogFragment() {

    companion object {
        private const val EMPTY_RES_ID = 0

        const val REQUEST_KEY_GENERIC_ALERT = "genericAlert"
        const val KEY_RESULT = "result"
    }

    private val navArgs: GenericAlertDialogArgs by navArgs()

    private val onDialogButtonClickListener =
        DialogInterface.OnClickListener { _, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    onPositiveButtonClicked()
                }
                DialogInterface.BUTTON_NEGATIVE -> {
                    onNegativeButtonClicked()
                }
                DialogInterface.BUTTON_NEUTRAL -> {
                    onNeutralButtonClicked()
                }
            }
        }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext(), R.style.MainAlertDialogStyle).run {
            if (navArgs.titleResId != EMPTY_RES_ID) {
                setTitle(navArgs.titleResId)
            } else if (navArgs.titleString != null) {
                setTitle(navArgs.titleString)
            }
            if (navArgs.messageResId != EMPTY_RES_ID) {
                setMessage(navArgs.messageResId)
            } else if (navArgs.messageString != null) {
                setMessage(navArgs.messageString)
            }
            if (navArgs.positiveButtonResId != EMPTY_RES_ID) {
                setPositiveButton(navArgs.positiveButtonResId, onDialogButtonClickListener)
            } else if (navArgs.positiveButtonString != null) {
                setPositiveButton(navArgs.positiveButtonString, onDialogButtonClickListener)
            }
            if (navArgs.negativeButtonResId != EMPTY_RES_ID) {
                setNegativeButton(navArgs.negativeButtonResId, onDialogButtonClickListener)
            } else if (navArgs.negativeButtonString != null) {
                setNegativeButton(navArgs.negativeButtonString, onDialogButtonClickListener)
            }
            if (navArgs.neutralButtonResId != EMPTY_RES_ID) {
                setNeutralButton(navArgs.neutralButtonResId, onDialogButtonClickListener)
            } else if (navArgs.neutralButtonString != null) {
                setNeutralButton(navArgs.neutralButtonString, onDialogButtonClickListener)
            }
            setCancelable(navArgs.isCancelable)
            create()
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        onCanceled()
    }

    private fun onPositiveButtonClicked() {
        parentFragmentManager.setFragmentResult(
            REQUEST_KEY_GENERIC_ALERT,
            bundleOf(
                KEY_RESULT to GenericAlertDialogResult.POSITIVE
            )
        )
        findNavController().popBackStack()
    }

    private fun onNegativeButtonClicked() {
        parentFragmentManager.setFragmentResult(
            REQUEST_KEY_GENERIC_ALERT,
            bundleOf(
                KEY_RESULT to GenericAlertDialogResult.NEGATIVE
            )
        )
        findNavController().popBackStack()
    }

    private fun onNeutralButtonClicked() {
        parentFragmentManager.setFragmentResult(
            REQUEST_KEY_GENERIC_ALERT,
            bundleOf(
                KEY_RESULT to GenericAlertDialogResult.NEUTRAL
            )
        )
        findNavController().popBackStack()
    }

    private fun onCanceled() {
        parentFragmentManager.setFragmentResult(
            REQUEST_KEY_GENERIC_ALERT,
            bundleOf(
                KEY_RESULT to GenericAlertDialogResult.CANCELED
            )
        )
        findNavController().popBackStack()
    }

    enum class GenericAlertDialogResult {
        POSITIVE,
        NEGATIVE,
        NEUTRAL,
        CANCELED;
    }
}