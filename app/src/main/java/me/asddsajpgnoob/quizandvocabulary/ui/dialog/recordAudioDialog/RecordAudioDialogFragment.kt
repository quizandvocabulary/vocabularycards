package me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.*
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.collectLatest
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.databinding.DialogRecordAudioBinding
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseDialogFragment
import me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog.model.AudioViewState
import me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog.model.RecordAudioEvent
import me.asddsajpgnoob.quizandvocabulary.ui.main.MainActivity
import me.asddsajpgnoob.quizandvocabulary.util.DisplayMeasureUtils
import me.asddsajpgnoob.quizandvocabulary.util.IntentUtils


class RecordAudioDialogFragment : BaseDialogFragment<DialogRecordAudioBinding>() {

    companion object {
        const val REQUEST_KEY_RECORD_AUDIO = "recordAudio"

        const val KEY_AUDIO_FILE = "audioFileName"
    }

    private val viewModel: RecordAudioViewModel by viewModels {
        RecordAudioViewModel.Factory(requireContext().getExternalFilesDir(null)!!)
    }

    private val recordAudioPermissionResultLauncher: ActivityResultLauncher<String> =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                viewModel.onRecordAudioPermissionGranted()
            } else {
                Snackbar.make(
                    requireView(),
                    R.string.msg_audio_recording_permission_denied,
                    Snackbar.LENGTH_LONG
                ).run {
                    setAction(R.string.Settings) {
                        appSettingsResultLauncher.launch(IntentUtils.getAppSettingsIntent())
                    }
                    show()
                }
            }
        }

    private val appSettingsResultLauncher: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val result = requireContext().checkSelfPermission(Manifest.permission.RECORD_AUDIO)
                if (result == PackageManager.PERMISSION_GRANTED) {
                    viewModel.onRecordAudioPermissionGranted()
                }
            }
        }

    private val vibrator: Vibrator by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            (requireContext().getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager).defaultVibrator
        } else {
            @Suppress("deprecation")
            requireContext().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        }
    }

    override fun getViewBinding() = DialogRecordAudioBinding.inflate(layoutInflater)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        setOnClickListeners()
        setOnTouchListeners()

        collectRecordingTime()
        collectMediaPlayerProgress()
        collectViewState()

        collectEvents()

        return view
    }

    override fun isCancelable() = true

    override fun getHeight(): Int {
        val height = DisplayMeasureUtils.getDeviceSize(requireContext()).second

        return when (MainActivity.screenOrientation) {
            Configuration.ORIENTATION_LANDSCAPE -> {
                height - height / 10
            }
            else -> {
                (height * 0.8f).toInt()
            }
        }
    }

    override fun getWidth(): Int {
        val width = DisplayMeasureUtils.getDeviceSize(requireContext()).first

        return when (MainActivity.screenOrientation) {
            Configuration.ORIENTATION_LANDSCAPE -> {
                (width * 0.7f).toInt()
            }
            else -> {
                width - width / 10
            }
        }
    }

    private fun requestRecordAudioPermission() {
        recordAudioPermissionResultLauncher.launch(Manifest.permission.RECORD_AUDIO)
    }

    private fun setOnClickListeners() {
        binding.apply {
            textResetAudio.setOnClickListener {
                viewModel.onResetAudioClicked()
            }
            imageButtonPlayPause.setOnClickListener {
                viewModel.onPlayPauseClicked()
            }
            buttonSubmit.setOnClickListener {
                viewModel.onSubmitButtonClicked()
            }
            buttonCancel.setOnClickListener {
                viewModel.onCancelButtonClicked()
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setOnTouchListeners() {
        binding.imageButtonMic.setOnTouchListener { _, event ->
            return@setOnTouchListener when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    viewModel.onImageButtonMicHold()
                    true
                }
                MotionEvent.ACTION_UP -> {
                    viewModel.onImageButtonMicRelease()
                    true
                }
                else -> {
                    false
                }
            }
        }
    }

    private fun collectRecordingTime() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.recordingTime.collectLatest {
                binding.textRecordingDuration.text =
                    getString(R.string.number_seconds_unformatted, it.toFloat() / 1000f)
            }
        }
    }

    private fun collectMediaPlayerProgress() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.mediaPlayerProgress.collectLatest {
                binding.apply {
                    textPlayingPosition.text = getString(
                        R.string.number_slash_number_seconds_unformatted,
                        it.progress.toFloat() / 1000f,
                        it.duration.toFloat() / 1000f
                    )
                    progressRecordedShout.progress = it.progress
                }
            }
        }
    }

    private fun collectViewState() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.viewState.collectLatest {
                binding.apply {
                    when (it) {
                        AudioViewState.Initial -> {
                            textTopInfo.isVisible = true
                            textTopInfo.setText(R.string.Touch_and_hold_to_record)
                            textRecordingDuration.isVisible = false
                            textPlayingPosition.isVisible = false
                            imageButtonMic.isVisible = true
                            imageButtonMic.setBackgroundResource(R.drawable.background_circle_record_audio_orange)
                            layoutRecordedAudio.isVisible = false
                            progressRecordedShout.max = 100
                            progressRecordedShout.progress = 100
                            imageButtonPlayPause.setBackgroundResource(R.drawable.background_circle_record_audio_orange)
                            imageButtonPlayPause.setImageResource(R.drawable.ic_play_theme_white_24)
                            textResetAudio.isVisible = false
                            buttonSubmit.isEnabled = false
                            showWaves(false)
                        }
                        AudioViewState.Recording -> {
                            textTopInfo.isVisible = true
                            textTopInfo.setText(R.string.Recording)
                            textRecordingDuration.isVisible = true
                            textPlayingPosition.isVisible = false
                            imageButtonMic.isVisible = true
                            imageButtonMic.setBackgroundResource(R.drawable.circle_red)
                            layoutRecordedAudio.isVisible = false
                            textResetAudio.isVisible = false
                            buttonSubmit.isEnabled = false
                            showWaves(true)
                        }
                        is AudioViewState.RecordingCompleted -> {
                            textTopInfo.isVisible = false
                            textRecordingDuration.isVisible = true
                            textRecordingDuration.text =
                                getString(
                                    R.string.number_seconds_unformatted,
                                    it.duration.toFloat() / 1000f
                                )
                            textPlayingPosition.isVisible = false
                            imageButtonMic.isVisible = false
                            layoutRecordedAudio.isVisible = true
                            progressRecordedShout.max = it.duration
                            progressRecordedShout.progress = it.duration
                            imageButtonPlayPause.setBackgroundResource(R.drawable.background_circle_record_audio_orange)
                            imageButtonPlayPause.setImageResource(R.drawable.ic_play_theme_white_24)
                            textResetAudio.isVisible = true
                            buttonSubmit.isEnabled = true
                            showWaves(false)
                        }
                        is AudioViewState.Playing -> {
                            textTopInfo.isVisible = false
                            textRecordingDuration.isVisible = false
                            textPlayingPosition.isVisible = true
                            textPlayingPosition.text = getString(
                                R.string.number_slash_number_seconds_unformatted,
                                it.position.toFloat() / 1000f,
                                it.duration.toFloat() / 1000f
                            )
                            imageButtonMic.isVisible = false
                            layoutRecordedAudio.isVisible = true
                            progressRecordedShout.max = it.duration
                            progressRecordedShout.progress = it.position
                            imageButtonPlayPause.setBackgroundResource(R.drawable.circle_playing_recorded_audio_light_orange)
                            imageButtonPlayPause.setImageResource(R.drawable.ic_pause_theme_white_24)
                            textResetAudio.isVisible = true
                            buttonSubmit.isEnabled = true
                            showWaves(false)
                        }
                        is AudioViewState.Paused -> {
                            textTopInfo.isVisible = false
                            textRecordingDuration.isVisible = false
                            textPlayingPosition.isVisible = true
                            imageButtonMic.isVisible = false
                            layoutRecordedAudio.isVisible = true
                            progressRecordedShout.max = it.duration
                            progressRecordedShout.progress = it.position
                            imageButtonPlayPause.setBackgroundResource(R.drawable.circle_playing_recorded_audio_light_orange)
                            imageButtonPlayPause.setImageResource(R.drawable.ic_play_theme_white_24)
                            textResetAudio.isVisible = true
                            buttonSubmit.isEnabled = true
                            showWaves(false)
                        }
                    }
                }
            }
        }
    }

    private fun showWaves(show: Boolean) {
        binding.apply {
            waveView.isVisible = show
            waveView.isAnimating = show
            waveView.resetWaves()
        }
    }

    private fun collectEvents() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.events.collect { event ->
                when (event) {
                    is RecordAudioEvent.ShowToast -> {
                        showToast(event.stringRes)
                    }
                    RecordAudioEvent.Vibrate -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            vibrator.vibrate(
                                VibrationEffect.createOneShot(
                                    100,
                                    VibrationEffect.DEFAULT_AMPLITUDE
                                )
                            )
                        } else {
                            @Suppress("deprecation")
                            vibrator.vibrate(100)
                        }
                    }
                    RecordAudioEvent.RequestRecordAudioPermission -> {
                        requestRecordAudioPermission()
                    }
                    RecordAudioEvent.PopBackStack -> {
                        popBackStack()
                    }
                    is RecordAudioEvent.PopBackStackWithResult -> {
                        parentFragmentManager.setFragmentResult(
                            REQUEST_KEY_RECORD_AUDIO, bundleOf(
                                KEY_AUDIO_FILE to event.audioFile
                            )
                        )
                        popBackStack()
                    }
                }
            }
        }
    }

}