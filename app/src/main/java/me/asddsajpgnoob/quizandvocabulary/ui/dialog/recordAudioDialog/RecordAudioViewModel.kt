package me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog

import android.media.MediaPlayer
import android.media.MediaRecorder
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog.model.AudioViewState
import me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog.model.MediaPlayerProgress
import me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog.model.RecordAudioEvent
import me.asddsajpgnoob.quizandvocabulary.ui.quiz.QuizViewModel
import me.asddsajpgnoob.quizandvocabulary.util.AudioPlayerFactory
import me.asddsajpgnoob.quizandvocabulary.util.AudioRecorder
import me.asddsajpgnoob.quizandvocabulary.util.Constants
import me.asddsajpgnoob.quizandvocabulary.util.DateUtils
import java.io.File
import java.util.*

class RecordAudioViewModel(
    private val externalFilesDir: File
) : ViewModel() {

    private val viewStateMutable = MutableStateFlow<AudioViewState>(AudioViewState.Initial)
    val viewState = viewStateMutable.asStateFlow()

    private val recordingTimeMutable = MutableStateFlow(0L)
    val recordingTime = recordingTimeMutable.asStateFlow()

    private val mediaPlayerProgressMutable = MutableStateFlow(MediaPlayerProgress(0, 0))
    val mediaPlayerProgress = mediaPlayerProgressMutable.asStateFlow()

    private val eventsChannel = Channel<RecordAudioEvent>()
    val events = eventsChannel.receiveAsFlow()

    private var mediaPlayerPositionJob: Job? = null
    private var mediaPlayer: MediaPlayer? = null

    private var audioRecorder: AudioRecorder? = null

    private var shouldDeleteAudioInExit = true

    private var recordAudioPermissionGranted = false

    init {
        viewModelScope.launch {
            eventsChannel.send(RecordAudioEvent.RequestRecordAudioPermission)
        }
    }

    fun onRecordAudioPermissionGranted() {
        recordAudioPermissionGranted = true
    }

    fun onImageButtonMicHold() {
        if (recordAudioPermissionGranted) {
            if (audioRecorder == null) {
                initAudioAacRecorder()
            }
            ifNotRecordingStartIt()
            return
        }
        viewModelScope.launch {
            eventsChannel.send(RecordAudioEvent.RequestRecordAudioPermission)
        }
    }

    fun onImageButtonMicRelease() {
        ifRecordingStopIt()
    }

    fun onResetAudioClicked() {
        resetAudio()
    }

    fun onPlayPauseClicked() {
        if (isMediaPlayerPlaying()) {
            pauseMediaPlayer()
            return
        }

        startMediaPlayer()
    }

    fun onSubmitButtonClicked() {
        shouldDeleteAudioInExit = false
        viewModelScope.launch {
            if (audioRecorder?.isRecording == true) {
                eventsChannel.send(RecordAudioEvent.ShowToast(R.string.Still_recording))
                return@launch
            }
            val audioFile = audioRecorder?.outputPath?.let {
                File(it)
            }
            if (audioFile == null) {
                eventsChannel.send(RecordAudioEvent.ShowToast(R.string.No_recorded_audio))
                return@launch
            }
            if (isMediaPlayerPlaying()) {
                pauseMediaPlayer()
            }
            eventsChannel.send(RecordAudioEvent.PopBackStackWithResult(audioFile))
        }
    }

    fun onCancelButtonClicked() {
        if (audioRecorder?.isRecording == true) {
            ifRecordingStopIt()
        }
        if (isMediaPlayerPlaying()) {
            pauseMediaPlayer()
        }
        viewModelScope.launch {
            eventsChannel.send(RecordAudioEvent.PopBackStack)
        }
    }

    private fun ifNotRecordingStartIt() {
        if (!audioRecorder!!.isRecording) {
            audioRecorder!!.removeLastOutput()
            audioRecorder!!.startRecording()
            viewStateMutable.value = AudioViewState.Recording
            viewModelScope.launch {
                eventsChannel.send(RecordAudioEvent.Vibrate)
            }
        }
    }

    private fun ifRecordingStopIt() {
        if (audioRecorder?.isRecording == true) {
            viewModelScope.launch {
                var successFulStop = true
                while (isActive) {
                    val success = if (recordingTime.value > 499L) {
                        audioRecorder!!.stopRecording()
                        true
                    } else {
                        successFulStop = false
                        false
                    }
                    if (success) {
                        break
                    } else {
                        delay(100L)
                    }
                }
                if (successFulStop) {
                    initMediaPlayer(File(audioRecorder!!.outputPath!!))
                    viewStateMutable.value =
                        AudioViewState.RecordingCompleted(mediaPlayer!!.duration)
                    eventsChannel.send(RecordAudioEvent.Vibrate)
                } else {
                    viewStateMutable.value = AudioViewState.Initial
                    eventsChannel.send(RecordAudioEvent.ShowToast(R.string.Recording_must_be_at_least_half_second))
                }
            }
        }
    }

    private fun isMediaPlayerPlaying() = mediaPlayer?.isPlaying == true

    private fun startMediaPlayer() {
        if (viewStateMutable.value is AudioViewState.RecordingCompleted) {
            viewModelScope.launch {
                eventsChannel.send(RecordAudioEvent.Vibrate)
            }
        }
        viewStateMutable.value =
            AudioViewState.Playing(mediaPlayer!!.duration, mediaPlayer!!.currentPosition)
        mediaPlayerPositionJob = viewModelScope.launch {
            while (isActive) {
                mediaPlayerProgressMutable.value = MediaPlayerProgress(
                    mediaPlayer!!.currentPosition,
                    mediaPlayer!!.duration
                )
                delay(20L)
            }
        }
        mediaPlayer!!.start()
    }

    private fun resetAudio() {
        releaseMediaPlayer()
        audioRecorder!!.removeLastOutput()
        viewStateMutable.value = AudioViewState.Initial
        viewModelScope.launch {
            eventsChannel.send(RecordAudioEvent.Vibrate)
        }
    }

    private fun pauseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer!!.pause()
        viewStateMutable.value =
            AudioViewState.Paused(mediaPlayer!!.duration, mediaPlayer!!.currentPosition)
    }

    private fun initMediaPlayer(audioFile: File) {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer = AudioPlayerFactory.newInstance(audioFile)
        mediaPlayer!!.prepare()
        mediaPlayer!!.setOnCompletionListener {
            mediaPlayerPositionJob?.cancel()
            initMediaPlayer(audioFile)
            viewStateMutable.value = AudioViewState.RecordingCompleted(mediaPlayer!!.duration)
            viewModelScope.launch {
                eventsChannel.send(RecordAudioEvent.Vibrate)
            }
        }
    }

    private fun releaseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        if (isMediaPlayerPlaying()) {
            mediaPlayer!!.stop()
        }
        mediaPlayer?.release()
        mediaPlayer = null
    }

    private fun initAudioAacRecorder() {
        val audioRecordingsDirectory =
            File("${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}")
        if (!audioRecordingsDirectory.isDirectory) {
            audioRecordingsDirectory.mkdir()
        }
        audioRecorder = AudioRecorder.Builder()
            .setAudioSource(MediaRecorder.AudioSource.MIC)
            .setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS)
            .setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            .setCoroutineScope(viewModelScope)
            .saveRecordingsToDirectory(audioRecordingsDirectory)
            .setFileNameGenerator {
                "${UUID.randomUUID()}_${DateUtils.dateToString(Date(), pattern = DateUtils.FILE_NAME_FORMAT)}"
            }
            .build()
        audioRecorder!!.setOnRecordingTimeChangesListener { milliseconds ->
            recordingTimeMutable.value = milliseconds
        }
    }

    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()
        audioRecorder?.let {
            if (it.isRecording) {
                it.stopRecording()
            }
            if (shouldDeleteAudioInExit) {
                it.removeLastOutput()
            }
        }
    }

    class Factory(
        private val externalFilesDir: File
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(RecordAudioViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return RecordAudioViewModel(externalFilesDir) as T
            } else {
                throw IllegalArgumentException("unable to create ${QuizViewModel::class.qualifiedName}")
            }
        }

    }
}