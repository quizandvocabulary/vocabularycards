package me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog.model

sealed interface AudioViewState {
    object Initial : AudioViewState

    object Recording : AudioViewState

    data class RecordingCompleted(val duration: Int) : AudioViewState

    data class Playing(val duration: Int, val position: Int) : AudioViewState

    data class Paused(val duration: Int, val position: Int) : AudioViewState

}