package me.asddsajpgnoob.quizandvocabulary.ui.dialog.recordAudioDialog.model

import androidx.annotation.StringRes
import java.io.File

sealed interface RecordAudioEvent {

    data class ShowToast(@StringRes val stringRes: Int) : RecordAudioEvent

    object RequestRecordAudioPermission : RecordAudioEvent

    object Vibrate : RecordAudioEvent

    data class PopBackStackWithResult(val audioFile: File) : RecordAudioEvent

    object PopBackStack : RecordAudioEvent

}