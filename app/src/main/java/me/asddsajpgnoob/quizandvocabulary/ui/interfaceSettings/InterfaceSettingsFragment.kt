package me.asddsajpgnoob.quizandvocabulary.ui.interfaceSettings

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collectLatest
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.databinding.FragmentInterfaceSettingsBinding
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabulary.ui.main.MainViewModel

class InterfaceSettingsFragment : BaseFragment<FragmentInterfaceSettingsBinding>() {

    private val viewModel: InterfaceSettingsViewModel by viewModels {
        InterfaceSettingsViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentInterfaceSettingsBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        setOnClickListeners()

        collectDarkMode()
        collectDefaultTab()
        collectShowAudiosInManageScreen()
        collectShowExamplesInManageScreen()
        collectOpenTranslatorOnTopOfApp()
        collectSaveNavigationState()

        return view
    }

    private fun collectDarkMode() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.darkMode.collectLatest {
                when (it) {
                    DarkModePreferences.DARK -> {
                        binding.radioButtonDark.isChecked = true
                    }
                    DarkModePreferences.LIGHT -> {
                        binding.radioButtonLight.isChecked = true
                    }
                    DarkModePreferences.FOLLOW_SYSTEM -> {
                        binding.radioButtonDeviceTheme.isChecked = true
                    }
                }
            }
        }
    }

    private fun collectDefaultTab() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.defaultTab.collectLatest {
                when (it) {
                    DefaultTabPreferences.QUIZ -> {
                        binding.radioButtonQuiz.isChecked = true
                    }
                    DefaultTabPreferences.VOCABULARY -> {
                        binding.radioButtonVocabulary.isChecked = true
                    }
                }
            }
        }
    }

    private fun collectShowAudiosInManageScreen() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.showAudiosInVocabularyScreen.collectLatest {
                binding.checkBoxShowAudios.isChecked = it
            }
        }
    }

    private fun collectShowExamplesInManageScreen() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.showExamplesInVocabularyScreen.collectLatest {
                binding.checkBoxShowExamples.isChecked = it
            }
        }
    }

    private fun collectOpenTranslatorOnTopOfApp() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.openTranslatorOnTopOfApp.collectLatest {
                binding.checkBoxOpenTranslatorOnTopOfApp.isChecked = it
            }
        }
    }

    private fun collectSaveNavigationState() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.saveNavigationStateWhenSwitchingTab.collectLatest {
                binding.checkBoxSaveNavigationState.isChecked = it
            }
        }
    }

    private fun setOnClickListeners() {
        binding.apply {
            radioButtonDark.setOnClickListener {
                viewModel.onRadioButtonDarkClicked()
            }
            radioButtonLight.setOnClickListener {
                viewModel.onRadioButtonLightClicked()
            }
            radioButtonDeviceTheme.setOnClickListener {
                viewModel.onRadioButtonDeviceThemeClicked()
            }
            radioButtonVocabulary.setOnClickListener {
                viewModel.onRadioButtonVocabularyClicked()
            }
            radioButtonQuiz.setOnClickListener {
                viewModel.onRadioButtonQuizClicked()
            }
            checkBoxShowExamples.setOnClickListener {
                viewModel.onCheckboxShowExamplesClicked(checkBoxShowExamples.isChecked)
            }
            checkBoxShowAudios.setOnClickListener {
                viewModel.onCheckBoxShowAudiosClicked(checkBoxShowAudios.isChecked)
            }
            checkBoxOpenTranslatorOnTopOfApp.setOnClickListener {
                viewModel.onCheckBoxOpenTranslatorOnTopOfAppClicked(checkBoxOpenTranslatorOnTopOfApp.isChecked)
            }
            checkBoxSaveNavigationState.setOnClickListener {
                viewModel.onCheckboxSaveNavigationStateClicked(checkBoxSaveNavigationState.isChecked)
            }
        }
    }

}