package me.asddsajpgnoob.quizandvocabulary.ui.interfaceSettings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabulary.data.repository.InterfaceSettingsRepository

class InterfaceSettingsViewModel(
    private val interfaceSettingsRepository: InterfaceSettingsRepository
) : ViewModel() {

    val darkMode = interfaceSettingsRepository.darkMode

    val defaultTab = interfaceSettingsRepository.defaultTab

    val showAudiosInVocabularyScreen = interfaceSettingsRepository.showAudiosInVocabularyScreen

    val showExamplesInVocabularyScreen = interfaceSettingsRepository.showExamplesInVocabularyScreen

    val openTranslatorOnTopOfApp = interfaceSettingsRepository.openTranslatorOnTopOfApp

    val saveNavigationStateWhenSwitchingTab =
        interfaceSettingsRepository.saveNavigationStateWhenSwitchingTab

    fun onRadioButtonDarkClicked() = viewModelScope.launch {
        interfaceSettingsRepository.setDarkMode(DarkModePreferences.DARK)
    }

    fun onRadioButtonLightClicked() = viewModelScope.launch {
        interfaceSettingsRepository.setDarkMode(DarkModePreferences.LIGHT)
    }

    fun onRadioButtonDeviceThemeClicked() = viewModelScope.launch {
        interfaceSettingsRepository.setDarkMode(DarkModePreferences.FOLLOW_SYSTEM)
    }

    fun onRadioButtonVocabularyClicked() = viewModelScope.launch {
        interfaceSettingsRepository.setDefaultTab(DefaultTabPreferences.VOCABULARY)
    }

    fun onRadioButtonQuizClicked() = viewModelScope.launch {
        interfaceSettingsRepository.setDefaultTab(DefaultTabPreferences.QUIZ)
    }

    fun onCheckBoxShowAudiosClicked(isChecked: Boolean) = viewModelScope.launch {
        interfaceSettingsRepository.setShowAudiosInVocabularyScreen(isChecked)
    }

    fun onCheckboxShowExamplesClicked(isChecked: Boolean) = viewModelScope.launch {
        interfaceSettingsRepository.setExamplesAudiosInVocabularyScreen(isChecked)
    }

    fun onCheckBoxOpenTranslatorOnTopOfAppClicked(isChecked: Boolean) =
        viewModelScope.launch {
            interfaceSettingsRepository.setOpenTranslatorOnTopOfApp(isChecked)
        }

    fun onCheckboxSaveNavigationStateClicked(isChecked: Boolean) = viewModelScope.launch {
        interfaceSettingsRepository.setSaveNavigationStateWhenSwitchingTab(isChecked)
    }

    class Factory(
        private val interfaceSettingsRepository: InterfaceSettingsRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(InterfaceSettingsViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                InterfaceSettingsViewModel(interfaceSettingsRepository) as T
            } else {
                throw IllegalArgumentException("unable to create ${InterfaceSettingsViewModel::class.qualifiedName}")
            }
        }

    }
}