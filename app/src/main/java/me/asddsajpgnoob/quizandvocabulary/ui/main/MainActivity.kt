package me.asddsajpgnoob.quizandvocabulary.ui.main

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import kotlinx.coroutines.flow.collectLatest
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.databinding.ActivityMainBinding
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseActivity
import me.asddsajpgnoob.quizandvocabulary.util.UiStateUtils
import me.asddsajpgnoob.quizandvocabulary.util.UiStateUtils.setLightSystemBars
import me.asddsajpgnoob.quizandvocabulary.util.customSetupWithNavController

class MainActivity : BaseActivity<ActivityMainBinding>() {

    companion object {
        var screenOrientation = Configuration.ORIENTATION_PORTRAIT
            private set
    }

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    private val viewModel: MainViewModel by viewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = ActivityMainBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindView()

        screenOrientation = resources.configuration.orientation

        setSupportActionBar(binding.toolbar)

        val sysBarColor = ContextCompat.getColor(this, R.color.theme_system_bar_color)
        window.statusBarColor = sysBarColor
        window.navigationBarColor = sysBarColor

        collectDarkMode()
        collectScreenOrientation()

        setupNavigationComponent()
    }

    private fun setupNavigationComponent() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        navController = navHostFragment.findNavController()

        var graph: NavGraph? = viewModel.graph
        if (graph == null) {
            val startDestinationId = when (viewModel.defaultTab) {
                DefaultTabPreferences.QUIZ -> {
                    R.id.navigation_quiz
                }
                DefaultTabPreferences.VOCABULARY -> {
                    R.id.navigation_vocabulary
                }
            }
            graph = navController.navInflater.inflate(R.navigation.mobile_navigation)
            graph.setStartDestination(startDestinationId)
        }

        navController.graph = graph
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_quiz,
                R.id.navigation_vocabulary
            )
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.customSetupWithNavController(navController) {
            viewModel.currentSaveNavigationState
        }
    }

    private fun collectDarkMode() {
        lifecycleScope.launchWhenStarted {
            viewModel.darkMode.collectLatest {
                when (it) {
                    DarkModePreferences.DARK -> {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                        setLightSystemBars(binding.root, false)
                    }
                    DarkModePreferences.LIGHT -> {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                        setLightSystemBars(binding.root,true)
                    }
                    DarkModePreferences.FOLLOW_SYSTEM -> {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                        setLightSystemBars(binding.root, !UiStateUtils.isNightMode(resources))
                    }
                }
            }
        }
    }

    private fun collectScreenOrientation() {
        lifecycleScope.launchWhenStarted {
            viewModel.orientation.collectLatest {
                requestedOrientation = it
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.saveGraph(navController.graph)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}