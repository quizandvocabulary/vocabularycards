package me.asddsajpgnoob.quizandvocabulary.ui.main

import android.content.pm.ActivityInfo
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavGraph
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabulary.data.repository.InterfaceSettingsRepository

class MainViewModel(
    private val interfaceSettingsRepository: InterfaceSettingsRepository
) : ViewModel() {

    val darkMode: Flow<DarkModePreferences> = interfaceSettingsRepository.darkMode

    private val saveNavigationStateWhenSwitchingTab = interfaceSettingsRepository.saveNavigationStateWhenSwitchingTab

    val defaultTab: DefaultTabPreferences = runBlocking {
        interfaceSettingsRepository.defaultTab.first()
    }

    var currentSaveNavigationState: Boolean = runBlocking {
        interfaceSettingsRepository.saveNavigationStateWhenSwitchingTab.first()
    }
        private set

    var graph: NavGraph? = null
        private set

    init {
        viewModelScope.launch {
            saveNavigationStateWhenSwitchingTab.collectLatest {
                currentSaveNavigationState = it
            }
        }
    }

    private val _orientation = MutableStateFlow(
        when (defaultTab) {
            DefaultTabPreferences.QUIZ -> {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }
            DefaultTabPreferences.VOCABULARY -> {
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
            }
        }
    )
    val orientation = _orientation.asStateFlow()

    fun setOrientation(activityInfo: Int) {
        _orientation.value = activityInfo
    }

    fun saveGraph(graph: NavGraph) {
        this.graph = graph
    }

    class Factory(private val interfaceSettingsRepository: InterfaceSettingsRepository) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                MainViewModel(interfaceSettingsRepository) as T
            } else {
                throw IllegalArgumentException("unable to create ${MainViewModel::class.qualifiedName}")
            }
        }
    }
}