package me.asddsajpgnoob.quizandvocabulary.ui.quiz

import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.*
import androidx.core.animation.addListener
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collectLatest
import me.asddsajpgnoob.quizandvocabulary.MobileNavigationDirections
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.OnTimeActionRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.QuizSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.VocabularyCardRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.databinding.FragmentQuizBinding
import me.asddsajpgnoob.quizandvocabulary.ui.addEditVocabularyCard.AddEditVocabularyCardFragment
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabulary.ui.dialog.genericAlertDialog.GenericAlertDialog
import me.asddsajpgnoob.quizandvocabulary.ui.main.MainViewModel
import me.asddsajpgnoob.quizandvocabulary.ui.quiz.model.DragDirection
import me.asddsajpgnoob.quizandvocabulary.ui.quiz.model.QuizState
import me.asddsajpgnoob.quizandvocabulary.ui.quiz.model.QuizEvent
import me.asddsajpgnoob.quizandvocabulary.util.DisplayMeasureUtils
import me.asddsajpgnoob.quizandvocabulary.util.TouchUtils
import me.asddsajpgnoob.quizandvocabulary.util.hideKeyboard
import kotlin.math.absoluteValue

@SuppressLint("ClickableViewAccessibility")
class QuizFragment : BaseFragment<FragmentQuizBinding>() {

    companion object {
        private const val CURRENT_DESTINATION_ID = R.id.navigation_quiz

        private const val ANIMATOR_DURATION = 250L
        private const val FINAL_ROTATION_DEGREE = 15f
    }

    private val viewModel: QuizViewModel by viewModels {
        QuizViewModel.Factory(
            VocabularyCardRepositoryImpl,
            OnTimeActionRepositoryImpl,
            QuizSettingsRepositoryImpl,
            requireContext().getExternalFilesDir(null)!!
        )
    }

    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    private var quizCardDragDirection = DragDirection.NONE

    private var disappearRightAnimator: AnimatorSet? = null
    private var disappearLeftAnimator: AnimatorSet? = null
    private var resetQuizCardAnimator: AnimatorSet? = null

    private lateinit var deviceSize: Pair<Int, Int>
    private var isRtl = false

    override fun getViewBinding() = FragmentQuizBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        requireActivity().hideKeyboard(true)

        setFragmentResultListeners()

        setViewDefaultParams()
        setOnAnimationListeners()
        setOnClickListeners()

        collectQuizState()
        collectPlaybackInfo()

        collectEvents()

        return view
    }

    private fun setFragmentResultListeners() {
        parentFragmentManager.setFragmentResultListener(
            GenericAlertDialog.REQUEST_KEY_GENERIC_ALERT,
            this
        ) { _, bundle ->
            viewModel.onGenericAlertDialogResult(bundle.getSerializable(GenericAlertDialog.KEY_RESULT) as GenericAlertDialog.GenericAlertDialogResult)
        }
    }

    private fun setViewDefaultParams() {
        quizCardDragDirection = DragDirection.NONE
        deviceSize = DisplayMeasureUtils.getDeviceSize(requireContext())
        isRtl = resources.getBoolean(R.bool.isRTL)
    }

    private fun createQuizCardOnTouchListener(): View.OnTouchListener {
        return object : View.OnTouchListener {
            private var touchCanBeClick = false

            private var startX = 0f
            private var startY = 0f

            private val clickThreshold = DisplayMeasureUtils.dpToPx(requireContext(), 70f)

            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (binding.quizCard.isAnimating) {
                    return true
                }

                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (startX != 0f && startY != 0f) {
                            return true
                        }

                        startX = event.rawX
                        startY = event.rawY
                        touchCanBeClick = binding.quizCard.isFirstVisible
                        resetQuizCardPropsAndDragDirection()

                        return true
                    }
                    MotionEvent.ACTION_UP -> {
                        if (binding.quizCard.translationX.absoluteValue > deviceSize.first * 0.3f) {
                            animateDisappearQuizCard()
                        } else if (touchCanBeClick && binding.quizCard.isFirstVisible) {
                            binding.quizCard.animateChangeSide()

                            resetQuizCardPropsAndDragDirection()
                        } else {
                            animateResetQuizCardPropsAndDragDirection()
                        }

                        startX = 0f
                        startY = 0f
                        touchCanBeClick = false

                        return true
                    }
                    MotionEvent.ACTION_CANCEL -> {
                        startX = 0f
                        startY = 0f
                        touchCanBeClick = false
                        animateResetQuizCardPropsAndDragDirection()

                        return true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        val x: Float = event.rawX
                        val y: Float = event.rawY

                        if (touchCanBeClick) {
                            if (TouchUtils.isInClickArea(startX, x, startY, y, clickThreshold)) {
                                return true
                            } else {
                                touchCanBeClick = false
                                resetQuizCardPropsAndDragDirection()
                            }
                        }
                        if (!touchCanBeClick && !binding.quizCard.isFirstVisible) {
                            val diff: Float = x - startX
                            binding.quizCard.translationX = diff * 1.5f

                            val coefficient = diff / deviceSize.first
                            binding.quizCard.rotation = coefficient * FINAL_ROTATION_DEGREE

                            binding.quizCard.alpha = 1f - coefficient.absoluteValue
                            quizCardDragDirection = when {
                                diff > 0f -> {
                                    DragDirection.RIGHT
                                }
                                diff < 0f -> {
                                    DragDirection.LEFT
                                }
                                else -> {
                                    DragDirection.NONE
                                }
                            }
                        }

                        return true
                    }
                    else -> {
                        return false
                    }
                }
            }
        }
    }

    private fun animateDisappearQuizCard() {
        when (quizCardDragDirection) {
            DragDirection.RIGHT -> {
                disappearRightAnimator = createDisappearRightAnimator()
                disappearRightAnimator!!.start()
            }
            DragDirection.LEFT -> {
                disappearLeftAnimator = createDisappearLeftAnimator()
                disappearLeftAnimator!!.start()
            }
            DragDirection.NONE -> {
                resetQuizCardPropsAndDragDirection()
            }
        }
    }

    private fun animateResetQuizCardPropsAndDragDirection() {
        disappearLeftAnimator?.cancel()
        disappearRightAnimator?.cancel()
        resetQuizCardAnimator?.cancel()
        resetQuizCardAnimator = createResetQuizCardAnimation()
        resetQuizCardAnimator!!.start()
    }

    private fun resetQuizCardPropsAndDragDirection() {
        disappearLeftAnimator?.cancel()
        disappearRightAnimator?.cancel()
        resetQuizCardAnimator?.cancel()
        binding.apply {
            quizCardDragDirection = DragDirection.NONE
            quizCard.alpha = 1f
            quizCard.rotation = 0f
            quizCard.translationX = 0f
        }
    }

    private fun createDisappearRightAnimator(): AnimatorSet {
        val translate =
            ValueAnimator.ofFloat(binding.quizCard.translationX, deviceSize.first.toFloat())
                .apply {
                    duration = ANIMATOR_DURATION
                    addUpdateListener {
                        binding.quizCard.translationX = it.animatedValue as Float
                    }
                }
        val rotation =
            ValueAnimator.ofFloat(binding.quizCard.rotation, FINAL_ROTATION_DEGREE).apply {
                duration = ANIMATOR_DURATION
                addUpdateListener {
                    binding.quizCard.rotation = it.animatedValue as Float
                }
            }
        val alpha = ValueAnimator.ofFloat(binding.quizCard.alpha, 0f).apply {
            duration = ANIMATOR_DURATION
            addUpdateListener {
                binding.quizCard.alpha = it.animatedValue as Float
            }
        }
        return AnimatorSet().apply {
            playTogether(translate, rotation, alpha)
            addListener(onEnd = {
//                resetQuizCardPropsAndDragDirection()
                val guessed = !isRtl
                viewModel.onQuizCardDisappeared(guessed)
            })
        }
    }

    private fun createDisappearLeftAnimator(): AnimatorSet {
        val translate =
            ValueAnimator.ofFloat(binding.quizCard.translationX, deviceSize.first.toFloat() * -1f)
                .apply {
                    duration = ANIMATOR_DURATION
                    addUpdateListener {
                        binding.quizCard.translationX = it.animatedValue as Float
                    }
                }
        val rotation =
            ValueAnimator.ofFloat(binding.quizCard.rotation, -FINAL_ROTATION_DEGREE).apply {
                duration = ANIMATOR_DURATION
                addUpdateListener {
                    binding.quizCard.rotation = it.animatedValue as Float
                }
            }
        val alpha = ValueAnimator.ofFloat(binding.quizCard.alpha, 0f).apply {
            duration = ANIMATOR_DURATION
            addUpdateListener {
                binding.quizCard.alpha = it.animatedValue as Float
            }
        }
        return AnimatorSet().apply {
            playTogether(translate, rotation, alpha)
            addListener(onEnd = {
//                resetQuizCardPropsAndDragDirection()
                val guessed = isRtl
                viewModel.onQuizCardDisappeared(guessed)
            })
        }
    }

    private fun createResetQuizCardAnimation(): AnimatorSet {
        val translate =
            ValueAnimator.ofFloat(binding.quizCard.translationX, 0f)
                .apply {
                    duration = ANIMATOR_DURATION
                    addUpdateListener {
                        binding.quizCard.translationX = it.animatedValue as Float
                    }
                }
        val rotation = ValueAnimator.ofFloat(binding.quizCard.rotation, 0f).apply {
            duration = ANIMATOR_DURATION
            addUpdateListener {
                binding.quizCard.rotation = it.animatedValue as Float
            }
        }
        val alpha = ValueAnimator.ofFloat(binding.quizCard.alpha, 1f).apply {
            duration = ANIMATOR_DURATION
            addUpdateListener {
                binding.quizCard.alpha = it.animatedValue as Float
            }
        }
        return AnimatorSet().apply {
            playTogether(translate, rotation, alpha)
            addListener(onEnd = {
                resetQuizCardPropsAndDragDirection()
            })
        }
    }

    private fun setOnAnimationListeners() {
        binding.apply {
            quizCard.setQuizCardAnimationListener(onEnd = {
                viewModel.onQuizCardAnimationEnd(quizCard.isFirstVisible)
            })
        }
    }

    private fun setOnClickListeners() {
        binding.buttonRestart.setOnClickListener {
            viewModel.onButtonRestartClicked()
        }
    }


    private fun collectQuizState() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.quizState.collectLatest { quizState ->
                when (quizState) {
                    QuizState.Loading -> {
                        binding.apply {
                            hideQuizCard()

                            textCenter.setText(R.string.Loading_dots)
                            textCenter.isVisible = true

                            buttonRestart.isVisible = false
                        }
                    }
                    QuizState.AllPassed -> {
                        binding.apply {
                            hideQuizCard()

                            textCenter.setText(R.string.All_passed)
                            textCenter.isVisible = true

                            buttonRestart.isVisible = true
                        }
                    }
                    QuizState.NothingToShow -> {
                        binding.apply {
                            hideQuizCard()

                            textCenter.setText(R.string.Nothing_to_show_nln_Add_cards_to_Vocabulary)
                            textCenter.isVisible = true

                            buttonRestart.isVisible = false
                        }
                    }
                    is QuizState.Show -> {
                        binding.apply {
                            quizCard.setData(viewModel.playbackInfo.value, quizState.vocabularyCard)
                            quizCard.setOnTouchListener(createQuizCardOnTouchListener())
                            quizCard.setOnShowMoreClickListener {
                                viewModel.onShowMoreClicked()
                            }
                            quizCard.setOnPlayPauseListener { fileName ->
                                viewModel.onPlayPauseClicked(fileName)
                            }
                            quizCard.setOnShowInQuizCheckedChangeListener { isChecked ->
                                viewModel.onQuizCardShowInQuizCheckedChange(isChecked)
                            }
                            resetQuizCardPropsAndDragDirection()
                            quizCard.setSideIfNotAnimating(quizState.isShowingFirst)
                            quizCard.isVisible = true

                            textCenter.isVisible = false

                            buttonRestart.isVisible = false
                        }
                    }
                }
            }
        }
    }

    private fun hideQuizCard() {
        binding.apply {
            quizCard.setOnTouchListener(null)
            quizCard.setOnShowMoreClickListener(null)
            quizCard.setOnPlayPauseListener(null)
            quizCard.setOnShowInQuizCheckedChangeListener(null)
            resetQuizCardPropsAndDragDirection()
            quizCard.isVisible = false
        }
    }

    private fun collectPlaybackInfo() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.playbackInfo.collectLatest {
                binding.quizCard.updatePlaybackInfo(it)
            }
        }
    }

    private fun collectEvents() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.events.collect { event ->
                when (event) {
                    is QuizEvent.NavigateToAddEditVocabularyCardScreen -> {
                        val action =
                            QuizFragmentDirections.actionNavigationQuizToAddEditVocabularyCardFragment(
                                event.vocabularyCard
                            )
                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                    }
                    QuizEvent.NavigateToTouchHintDialog -> {
                        val action = MobileNavigationDirections.actionGlobalGenericAlertDialog(
                            titleResId = R.string.Hint,
                            messageResId = R.string.msg_touch_card_to_see_translation_hint,
                            positiveButtonResId = R.string.OK
                        )
                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                    }
                    QuizEvent.NavigateToSwipeHintDialog -> {
                        val action = MobileNavigationDirections.actionGlobalGenericAlertDialog(
                            titleResId = R.string.Hint,
                            messageResId = R.string.msg_swipe_card_hint,
                            positiveButtonResId = R.string.OK
                        )
                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                    }
                    is QuizEvent.ShowToast -> {
                        showToast(event.stringRes)
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_quiz, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.item_settings -> {
            val action = QuizFragmentDirections.actionNavigationQuizToSettingsFragment()
            navigateIfInDestination(CURRENT_DESTINATION_ID, action)
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

}