package me.asddsajpgnoob.quizandvocabulary.ui.quiz

import android.media.MediaPlayer
import androidx.annotation.StringRes
import androidx.lifecycle.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.QuizModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabulary.data.model.room.VocabularyCardEntity.Companion.toDomainList
import me.asddsajpgnoob.quizandvocabulary.data.repository.OnTimeActionRepository
import me.asddsajpgnoob.quizandvocabulary.data.repository.QuizSettingsRepository
import me.asddsajpgnoob.quizandvocabulary.data.repository.VocabularyCardRepository
import me.asddsajpgnoob.quizandvocabulary.eventBus.EventBus
import me.asddsajpgnoob.quizandvocabulary.eventBus.model.VocabularyCardEvent
import me.asddsajpgnoob.quizandvocabulary.ui.common.model.PlaybackInfo
import me.asddsajpgnoob.quizandvocabulary.ui.dialog.genericAlertDialog.GenericAlertDialog
import me.asddsajpgnoob.quizandvocabulary.ui.quiz.model.GenericAlertDialogState
import me.asddsajpgnoob.quizandvocabulary.ui.quiz.model.QuizState
import me.asddsajpgnoob.quizandvocabulary.ui.quiz.model.QuizEvent
import me.asddsajpgnoob.quizandvocabulary.util.AudioPlayerFactory
import me.asddsajpgnoob.quizandvocabulary.util.Constants
import java.io.File
import java.util.*

class QuizViewModel(
    private val vocabularyCardRepository: VocabularyCardRepository,
    private val onTimeActionRepository: OnTimeActionRepository,
    quizSettingsRepository: QuizSettingsRepository,
    private val externalFilesDir: File
) : ViewModel() {

    companion object {
        private const val LIMIT = 20
    }

    private val _playbackInfo = MutableStateFlow<PlaybackInfo?>(null)
    val playbackInfo = _playbackInfo.asStateFlow()

    private val _quizState = MutableStateFlow<QuizState>(QuizState.Loading)
    val quizState = _quizState.asStateFlow()

    private val showQuizTouchHint = onTimeActionRepository.showQuizTouchHint

    private val showQuizSwipeDirectionHint = onTimeActionRepository.showQuizSwipeDirectionHint

    private val quizMode = quizSettingsRepository.quizMode

    private val eventsChannel = Channel<QuizEvent>()
    val events = eventsChannel.receiveAsFlow()

    private val passedCardIds = mutableListOf<Long>()
    private val availableCards = mutableListOf<VocabularyCard>()

    private var offset = 0

    private var mediaPlayerPositionJob: Job? = null
    private var mediaPlayer: MediaPlayer? = null

    private var genericAlertDialogState: GenericAlertDialogState =
        GenericAlertDialogState.NotShowing

    private var currentQuizMode = QuizModePreferences.RANDOM

    init {
        viewModelScope.launch {
            quizMode.collect {
                currentQuizMode = it
                refreshQuiz()
            }
        }
        viewModelScope.launch {
            EventBus.vocabularyCardEvent.collectLatest { event ->
                when(event) {
                    is VocabularyCardEvent.CardCreated -> {
                        onCardCreated(event.card)
                    }
                    is VocabularyCardEvent.CardDeleted -> {
                        onCardDeleted(event.card)
                    }
                    is VocabularyCardEvent.CardEdited -> {
                        onCardEdited(event.editedCard)
                    }
                }
            }
        }
    }

    private fun refreshQuiz() {
        passedCardIds.clear()
        availableCards.clear()
        offset = 0
        when (currentQuizMode) {
            QuizModePreferences.RANDOM -> {
                loadNextRandomQuizCard()
            }
            QuizModePreferences.FIRST_LESS_GUESSED,
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM,
            QuizModePreferences.NEWEST_FIRST,
            QuizModePreferences.OLDEST_FIRST -> {
                loadPackOfCardsAndShowCard()
            }
        }
    }

    private fun loadPackOfCardsAndShowCard() = viewModelScope.launch {
        _quizState.value = QuizState.Loading
        val cards = when (currentQuizMode) {
            QuizModePreferences.RANDOM -> {
                throw RuntimeException("can't be random mode")
            }
            QuizModePreferences.FIRST_LESS_GUESSED -> {
                vocabularyCardRepository.findLessGuessedVocabularyCards(offset, LIMIT)
            }
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM -> {
                vocabularyCardRepository.findLessGuessedVocabularyCards(offset, LIMIT)
            }
            QuizModePreferences.NEWEST_FIRST -> {
                vocabularyCardRepository.findNewestVocabularyCards(offset, LIMIT)
            }
            QuizModePreferences.OLDEST_FIRST -> {
                vocabularyCardRepository.findOldestVocabularyCards(offset, LIMIT)
            }
        }
        if (cards.isEmpty()) {
            if (offset == 0) {
                _quizState.value = QuizState.NothingToShow
            } else {
                _quizState.value = QuizState.AllPassed
            }
            return@launch
        }
        availableCards.clear()
        availableCards.addAll(
            withContext(Dispatchers.Default) {
                cards.toDomainList()
            }
        )
        offset += LIMIT
        showFromAvailableCardsOrLoadPack()
    }

    private fun showFromAvailableCardsOrLoadPack() {
        val pickRandom = when (currentQuizMode) {
            QuizModePreferences.RANDOM -> {
                throw RuntimeException("can't be random mode")
            }
            QuizModePreferences.FIRST_LESS_GUESSED -> {
                false
            }
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM -> {
                true
            }
            QuizModePreferences.NEWEST_FIRST -> {
                false
            }
            QuizModePreferences.OLDEST_FIRST -> {
                false
            }
        }
        val nextCard = if (pickRandom) {
            availableCards.randomOrNull()
        } else {
            availableCards.firstOrNull()
        }
        if (nextCard != null) {
            sendShowTouchHintDialogEventIfNeeded()
            _quizState.value = QuizState.Show(nextCard, true)
            availableCards.remove(nextCard)
        } else {
            loadPackOfCardsAndShowCard()
        }
    }

    fun onGenericAlertDialogResult(result: GenericAlertDialog.GenericAlertDialogResult) =
        viewModelScope.launch {
            when (result) {
                GenericAlertDialog.GenericAlertDialogResult.POSITIVE -> {
                    when (genericAlertDialogState) {
                        GenericAlertDialogState.ShowingTouchHintDialog -> {
                            onTimeActionRepository.setShowQuizTouchHint(false)
                        }
                        GenericAlertDialogState.ShowingSwipeHintDialog -> {
                            onTimeActionRepository.setShowQuizSwipeDirectionHint(false)
                        }
                        GenericAlertDialogState.NotShowing -> {
                            // do not nothing
                        }
                    }
                }
                GenericAlertDialog.GenericAlertDialogResult.NEGATIVE -> {
                    throw RuntimeException("not handled such case")
                }
                GenericAlertDialog.GenericAlertDialogResult.NEUTRAL -> {
                    throw RuntimeException("not handled such case")
                }
                GenericAlertDialog.GenericAlertDialogResult.CANCELED -> {
                    // nothing to do
                }
            }
            genericAlertDialogState = GenericAlertDialogState.NotShowing
        }

    fun onButtonRestartClicked() {
        refreshQuiz()
    }

    fun onQuizCardDisappeared(guessed: Boolean) {
        releaseMediaPlayer()
        _quizState.value.let {
            if (it is QuizState.Show) {
                val oldCard = it.vocabularyCard
                val updatedCard = if (guessed) {
                    it.vocabularyCard.copy(
                        guessedCount = it.vocabularyCard.guessedCount + 1,
                        updatedAt = Date()
                    )
                } else {
                    it.vocabularyCard.copy(
                        notGuessedCount = it.vocabularyCard.notGuessedCount + 1,
                        updatedAt = Date()
                    )
                }
                updateQuizCardInDb(oldCard, updatedCard)
                if (currentQuizMode == QuizModePreferences.RANDOM) {
                    passedCardIds.add(it.vocabularyCard.id)
                }
            }
        }
        when (currentQuizMode) {
            QuizModePreferences.RANDOM -> {
                loadNextRandomQuizCard()
            }
            QuizModePreferences.FIRST_LESS_GUESSED,
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM,
            QuizModePreferences.NEWEST_FIRST,
            QuizModePreferences.OLDEST_FIRST -> {
                showFromAvailableCardsOrLoadPack()
            }
        }
    }

    fun onShowMoreClicked() = viewModelScope.launch {
        _quizState.value.let {
            if (it is QuizState.Show) {
                eventsChannel.send(QuizEvent.NavigateToAddEditVocabularyCardScreen(it.vocabularyCard))
            }
        }
    }

    fun onQuizCardAnimationEnd(currentFirstVisible: Boolean) {
        _quizState.value.let {
            if (it is QuizState.Show) {
                _quizState.value = it.copy(
                    isShowingFirst = currentFirstVisible
                )
                sendShowSwipeHintDialogEventIfNeeded()
            }
        }
    }

    fun onQuizCardShowInQuizCheckedChange(checked: Boolean) {
        _quizState.value.let {
            if (it is QuizState.Show) {
                _quizState.value = it.copy(
                    vocabularyCard = it.vocabularyCard.copy(
                        showInQuiz = checked
                    )
                )
            }
        }
    }

    fun onPlayPauseClicked(fileName: String) {
        val playingInfo = _playbackInfo.value
        if (playingInfo != null && playingInfo.fileName != fileName) {
            releaseMediaPlayer()
        }
        if (!isMediaPlayerPlaying()) {
            if (mediaPlayer == null) {
                val audioFile =
                    File("${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}/${fileName}")
                if (!audioFile.exists()) {
                    showToast(R.string.Audio_file_not_found)
                    return
                }
                initMediaPlayer(audioFile)
            }
            startMediaPlayer()
            return
        }
        pauseMediaPlayer()
    }

    private fun showToast(@StringRes stringRes: Int) = viewModelScope.launch {
        eventsChannel.send(QuizEvent.ShowToast(stringRes))
    }

    private fun sendShowSwipeHintDialogEventIfNeeded() = viewModelScope.launch {
        _quizState.value.let {
            if (it is QuizState.Show) {
                if (!it.isShowingFirst && showQuizSwipeDirectionHint.first()) {
                    genericAlertDialogState = GenericAlertDialogState.ShowingSwipeHintDialog
                    eventsChannel.send(QuizEvent.NavigateToSwipeHintDialog)
                }
            }
        }
    }

    private fun sendShowTouchHintDialogEventIfNeeded() = viewModelScope.launch {
        if (showQuizTouchHint.first()) {
            genericAlertDialogState = GenericAlertDialogState.ShowingTouchHintDialog
            eventsChannel.send(QuizEvent.NavigateToTouchHintDialog)
        }
    }

    private fun updateQuizCardInDb(oldCard: VocabularyCard, card: VocabularyCard) =
        viewModelScope.launch {
            vocabularyCardRepository.update(card.toEntity())
            EventBus.submitVocabularyCardEvent(VocabularyCardEvent.CardEdited(oldCard, card))
        }

    private fun loadNextRandomQuizCard() = viewModelScope.launch {
        _quizState.value = QuizState.Loading
        val card = vocabularyCardRepository.findRandomVocabularyCard(passedCardIds)
        val nextQuizState: QuizState
        if (card != null) {
            nextQuizState = QuizState.Show(card.toDomain(), true)
            sendShowTouchHintDialogEventIfNeeded()
        } else {
            nextQuizState = if (passedCardIds.isEmpty()) {
                QuizState.NothingToShow
            } else {
                QuizState.AllPassed
            }
        }
        _quizState.value = nextQuizState
    }

    private fun isMediaPlayerPlaying() = mediaPlayer?.isPlaying == true

    private fun releaseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        if (isMediaPlayerPlaying()) {
            mediaPlayer!!.stop()
        }
        mediaPlayer?.release()
        mediaPlayer = null
        _playbackInfo.value = null
    }

    private fun pauseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer!!.pause()
        _playbackInfo.value = _playbackInfo.value!!.copy(
            playing = false
        )
    }

    private fun initMediaPlayer(audioFile: File) {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer = AudioPlayerFactory.newInstance(audioFile)
        mediaPlayer!!.prepare()
        _playbackInfo.value = PlaybackInfo(
            false,
            audioFile.name,
            mediaPlayer!!.duration,
            0
        )
        mediaPlayer!!.setOnCompletionListener {
            releaseMediaPlayer()
            _playbackInfo.value = null
        }
    }

    private fun startMediaPlayer() {
        mediaPlayerPositionJob = viewModelScope.launch {
            while (isActive) {
                _playbackInfo.value = _playbackInfo.value!!.copy(
                    playing = true,
                    playingPosition = mediaPlayer!!.currentPosition
                )
                delay(20L)
            }
        }
        mediaPlayer!!.start()
    }

    private fun onCardEdited(editedCard: VocabularyCard) {
        val currentQuizState = _quizState.value
        if (currentQuizState is QuizState.Show) {
            if (currentQuizState.vocabularyCard.id == editedCard.id) {
                _quizState.value = QuizState.Show(editedCard, currentQuizState.isShowingFirst)
            }
        }
    }

    private fun onCardDeleted(deletedCard: VocabularyCard) {
        _quizState.value.let {
            if (it is QuizState.Show) {
                if (it.vocabularyCard == deletedCard) {
                    when (currentQuizMode) {
                        QuizModePreferences.RANDOM -> {
                            loadNextRandomQuizCard()
                        }
                        QuizModePreferences.FIRST_LESS_GUESSED,
                        QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM,
                        QuizModePreferences.NEWEST_FIRST,
                        QuizModePreferences.OLDEST_FIRST -> {
                            offset--
                            showFromAvailableCardsOrLoadPack()
                        }
                    }
                }
            }
        }
    }

    private fun onCardCreated(createdCard: VocabularyCard) {
        when(currentQuizMode) {
            QuizModePreferences.RANDOM,
            QuizModePreferences.FIRST_LESS_GUESSED,
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM,
            QuizModePreferences.OLDEST_FIRST -> {
                _quizState.value.let {
                    if (it is QuizState.NothingToShow) {
                        offset++
                        _quizState.value = QuizState.Show(createdCard, true)
                    }
                }
            }
            QuizModePreferences.NEWEST_FIRST -> {
                _quizState.value.let {
                    offset++
                    if (it is QuizState.NothingToShow) {
                        _quizState.value = QuizState.Show(createdCard, true)
                    } else {
                        availableCards.add(0, createdCard)
                    }
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()
    }

    class Factory(
        private val vocabularyCardRepository: VocabularyCardRepository,
        private val onTimeActionRepository: OnTimeActionRepository,
        private val quizSettingsRepository: QuizSettingsRepository,
        private val externalFilesDir: File
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(QuizViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                QuizViewModel(
                    vocabularyCardRepository,
                    onTimeActionRepository,
                    quizSettingsRepository,
                    externalFilesDir
                ) as T
            } else {
                throw IllegalArgumentException("unable to create ${QuizViewModel::class.qualifiedName}")
            }
        }
    }
}