package me.asddsajpgnoob.quizandvocabulary.ui.quiz.model

enum class DragDirection {
    RIGHT,
    LEFT,
    NONE;
}