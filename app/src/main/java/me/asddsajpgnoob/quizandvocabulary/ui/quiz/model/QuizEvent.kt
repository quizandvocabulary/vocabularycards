package me.asddsajpgnoob.quizandvocabulary.ui.quiz.model

import androidx.annotation.StringRes
import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard

sealed interface QuizEvent {
    data class NavigateToAddEditVocabularyCardScreen(val vocabularyCard: VocabularyCard) : QuizEvent

    object NavigateToTouchHintDialog : QuizEvent

    object NavigateToSwipeHintDialog : QuizEvent

    data class ShowToast(@StringRes val stringRes: Int) : QuizEvent
}