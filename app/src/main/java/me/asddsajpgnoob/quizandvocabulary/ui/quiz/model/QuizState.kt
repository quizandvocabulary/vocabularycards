package me.asddsajpgnoob.quizandvocabulary.ui.quiz.model

import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard

sealed interface QuizState {

    data class Show(val vocabularyCard: VocabularyCard, val isShowingFirst: Boolean) : QuizState

    object Loading : QuizState

    object AllPassed : QuizState

    object NothingToShow : QuizState
}