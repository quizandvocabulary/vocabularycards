package me.asddsajpgnoob.quizandvocabulary.ui.quizSettings

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.QuizModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.QuizSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.databinding.FragmentQuizSettingsBinding
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabulary.ui.main.MainViewModel

class QuizSettingsFragment : BaseFragment<FragmentQuizSettingsBinding>() {

    private val viewModel: QuizSettingsViewModel by viewModels {
        QuizSettingsViewModel.Factory(QuizSettingsRepositoryImpl)
    }
    
    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentQuizSettingsBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)

        setOnClickListeners()

        collectQuizMode()

        return root
    }

    private fun setOnClickListeners() {
        binding.apply {
            radioButtonRandom.setOnClickListener {
                viewModel.onRadioButtonRandomClicked()
            }
            radioButtonLessGuessedFirst.setOnClickListener {
                viewModel.onRadioButtonLessGuessedFirstClicked()
            }
            radioButtonLessGuessedFirstRandom.setOnClickListener {
                viewModel.onRadioButtonLessGuessedFirstRandomClicked()
            }
            radioButtonNewestFirst.setOnClickListener {
                viewModel.onRadioButtonNewestFirstClicked()
            }
            radioButtonOldestFirst.setOnClickListener {
                viewModel.onRadioButtonOldestFirst()
            }
        }
    }

    private fun collectQuizMode() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.quizMode.collect {
                binding.apply {
                    when(it) {
                        QuizModePreferences.RANDOM -> {
                            radioButtonRandom.isChecked = true
                        }
                        QuizModePreferences.FIRST_LESS_GUESSED -> {
                            radioButtonLessGuessedFirst.isChecked = true
                        }
                        QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM -> {
                            radioButtonLessGuessedFirstRandom.isChecked = true
                        }
                        QuizModePreferences.NEWEST_FIRST -> {
                            radioButtonNewestFirst.isChecked = true
                        }
                        QuizModePreferences.OLDEST_FIRST -> {
                            radioButtonOldestFirst.isChecked = true
                        }
                    }
                }
            }
        }
    }
}