package me.asddsajpgnoob.quizandvocabulary.ui.quizSettings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.asddsajpgnoob.quizandvocabulary.data.model.dataStore.QuizModePreferences
import me.asddsajpgnoob.quizandvocabulary.data.repository.QuizSettingsRepository

class QuizSettingsViewModel(
    private val quizSettingsRepository: QuizSettingsRepository
) : ViewModel() {

    val quizMode = quizSettingsRepository.quizMode

    fun onRadioButtonRandomClicked() {
        setQuizMode(QuizModePreferences.RANDOM)
    }

    fun onRadioButtonLessGuessedFirstClicked() {
        setQuizMode(QuizModePreferences.FIRST_LESS_GUESSED)
    }

    fun onRadioButtonLessGuessedFirstRandomClicked() {
        setQuizMode(QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM)
    }

    fun onRadioButtonNewestFirstClicked() {
        setQuizMode(QuizModePreferences.NEWEST_FIRST)
    }

    fun onRadioButtonOldestFirst() {
        setQuizMode(QuizModePreferences.OLDEST_FIRST)
    }

    private fun setQuizMode(quizModePreferences: QuizModePreferences) = viewModelScope.launch {
        quizSettingsRepository.setQuizMode(quizModePreferences)
    }

    class Factory(
        private val quizSettingsRepository: QuizSettingsRepository
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(QuizSettingsViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                QuizSettingsViewModel(quizSettingsRepository) as T
            } else {
                throw IllegalArgumentException("unable to create ${QuizSettingsViewModel::class.qualifiedName}")
            }
        }

    }
}