package me.asddsajpgnoob.quizandvocabulary.ui.settings

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import me.asddsajpgnoob.quizandvocabulary.BuildConfig
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.databinding.FragmentSettingsBinding
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabulary.ui.main.MainViewModel
import me.asddsajpgnoob.quizandvocabulary.ui.settings.model.SettingsEvent

class SettingsFragment : BaseFragment<FragmentSettingsBinding>() {

    companion object {
        private const val CURRENT_DESTINATION_ID = R.id.settingsFragment
    }

    private val viewModel: SettingsViewModel by viewModels()

    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentSettingsBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        setupViews()
        setOnClickListeners()

        collectEvents()
        return view
    }

    private fun setupViews() {
        binding.apply {
            textAppVersion.text = BuildConfig.VERSION_NAME
        }
    }

    private fun setOnClickListeners() {
        binding.apply {
            textInterface.setOnClickListener {
                viewModel.onInterfaceLabelClick()
            }
            textQuiz.setOnClickListener {
                viewModel.onQuizClicked()
            }
        }
    }

    private fun collectEvents() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.events.collect { event ->
                when(event) {
                    SettingsEvent.NavigateToInterfaceSettingsFragment -> {
                        val action = SettingsFragmentDirections.actionSettingsFragmentToInterfaceSettingsFragment()
                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                    }
                    SettingsEvent.NavigateToQuizSettingsFragment -> {
                        val action = SettingsFragmentDirections.actionSettingsFragmentToQuizSettingsFragment()
                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                    }
                }
            }
        }
    }

}