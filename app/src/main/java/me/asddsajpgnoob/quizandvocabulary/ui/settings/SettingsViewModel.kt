package me.asddsajpgnoob.quizandvocabulary.ui.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import me.asddsajpgnoob.quizandvocabulary.ui.settings.model.SettingsEvent

class SettingsViewModel : ViewModel() {

    private val eventsChannel = Channel<SettingsEvent>()
    val events = eventsChannel.receiveAsFlow()

    fun onInterfaceLabelClick()  = viewModelScope.launch {
        eventsChannel.send(SettingsEvent.NavigateToInterfaceSettingsFragment)
    }

    fun onQuizClicked() = viewModelScope.launch {
        eventsChannel.send(SettingsEvent.NavigateToQuizSettingsFragment)
    }
}