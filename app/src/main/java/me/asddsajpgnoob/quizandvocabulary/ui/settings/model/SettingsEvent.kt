package me.asddsajpgnoob.quizandvocabulary.ui.settings.model

sealed interface SettingsEvent {

    object NavigateToInterfaceSettingsFragment : SettingsEvent

    object NavigateToQuizSettingsFragment: SettingsEvent
}