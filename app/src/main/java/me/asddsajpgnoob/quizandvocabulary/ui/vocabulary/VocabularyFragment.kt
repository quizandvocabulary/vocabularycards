package me.asddsajpgnoob.quizandvocabulary.ui.vocabulary

import android.content.pm.ActivityInfo
import android.graphics.*
import android.os.Bundle
import android.view.*
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.collectLatest
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.data.repository.impl.VocabularyCardRepositoryImpl
import me.asddsajpgnoob.quizandvocabulary.databinding.FragmentVocabularyBinding
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabulary.ui.main.MainViewModel
import me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.adapter.VocabularyCardsAdapter
import me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.model.VocabularyEvent
import me.asddsajpgnoob.quizandvocabulary.util.BitmapUtils
import me.asddsajpgnoob.quizandvocabulary.util.DisplayMeasureUtils
import me.asddsajpgnoob.quizandvocabulary.util.recyclerView.RecyclerViewShowHideScrollListener

class VocabularyFragment : BaseFragment<FragmentVocabularyBinding>(),
    VocabularyCardsAdapter.VocabularyCardAdapterConnector {

    companion object {
        private const val CURRENT_DESTINATION_ID = R.id.navigation_vocabulary
    }

    private val cardsAdapter = VocabularyCardsAdapter(this)

    private lateinit var cardsLayoutManager: LinearLayoutManager

    private val viewModel: VocabularyViewModel by viewModels {
        VocabularyViewModel.Factory(
            InterfaceSettingsRepositoryImpl,
            VocabularyCardRepositoryImpl,
            requireContext().getExternalFilesDir(null)!!,
            resources.getInteger(R.integer.vocabulary_list_initial_load_size),
            resources.getInteger(R.integer.vocabulary_list_page_size),
            resources.getInteger(R.integer.vocabulary_list_max_items_size)
        )
    }

    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentVocabularyBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        setupRecyclerViews()
        setOnClickListeners()

        collectCardAndPlaybackInfo()
        collectListItems()

        collectEvents()

        return view
    }

    private fun setupRecyclerViews() {
        binding.apply {
            recyclerViewCards.setHasFixedSize(true)
            cardsLayoutManager = binding.recyclerViewCards.layoutManager as LinearLayoutManager

            recyclerViewCards.addOnScrollListener(object : RecyclerViewShowHideScrollListener() {

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (dy > 0) {
                        val itemCount = cardsLayoutManager.itemCount
                        val lastVisibleItemPos =
                            cardsLayoutManager.findLastVisibleItemPosition()
                        if (lastVisibleItemPos != RecyclerView.NO_POSITION) {
                            if (lastVisibleItemPos == itemCount - 1) {
                                viewModel.onListBottomReached()
                            }
                        }
                    } else if (dy < 0) {
                        val firstVisibleItemPosition =
                            cardsLayoutManager.findFirstVisibleItemPosition()
                        if (firstVisibleItemPosition == 0) {
                            viewModel.onListTopReached()
                        }
                    }
                }

                override fun show() {
                    binding.floatingButtonAdd.animate()
                        .translationY(0f)
                        .setInterpolator(DecelerateInterpolator(2f))
                        .start()
                }

                override fun hide() {
                    binding.floatingButtonAdd.animate()
                        .translationY(binding.floatingButtonAdd.height + 60f)
                        .setInterpolator(AccelerateInterpolator(2f))
                        .start()
                }
            })

            val bitmap = BitmapUtils.getBitmapFromVectorDrawable(
                requireContext(),
                R.drawable.ic_delete_forever_white_24
            )
            val bitmapSize = DisplayMeasureUtils.dpToPx(requireContext(), 24f)
            val cornerRadius = resources.getDimension(R.dimen.main_card_view_corner_radius)

            val itemTouchHelperCallback = object :
                ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    viewModel.onItemSwiped(cardsAdapter.getItem(viewHolder.adapterPosition))
                }

                override fun onChildDraw(
                    c: Canvas,
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    dX: Float,
                    dY: Float,
                    actionState: Int,
                    isCurrentlyActive: Boolean
                ) {
                    val itemView = viewHolder.itemView
                    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                        val backgroundPaint = Paint()
                        backgroundPaint.color =
                            ContextCompat.getColor(requireContext(), R.color.red)
                        c.drawRoundRect(
                            itemView.left.toFloat(),
                            itemView.top.toFloat(),
                            itemView.right.toFloat(),
                            itemView.bottom.toFloat(),
                            cornerRadius,
                            cornerRadius,
                            backgroundPaint
                        )
                        val bitmapPaint = Paint()
                        bitmapPaint.color = ContextCompat.getColor(requireContext(), R.color.white)
                        if (dX > 0) {
                            c.drawBitmap(
                                bitmap,
                                itemView.left.toFloat() + bitmapSize / 2f,
                                itemView.bottom.toFloat() - (itemView.bottom - itemView.top) / 2f - bitmapSize / 2f,
                                bitmapPaint
                            )
                        } else if (dX < 0) {
                            c.drawBitmap(
                                bitmap,
                                itemView.right.toFloat() - bitmapSize - bitmapSize / 2f,
                                itemView.bottom.toFloat() - (itemView.bottom - itemView.top) / 2f - bitmapSize / 2f,
                                bitmapPaint
                            )
                        }
                    }
                    super.onChildDraw(
                        c,
                        recyclerView,
                        viewHolder,
                        dX,
                        dY,
                        actionState,
                        isCurrentlyActive
                    )
                }

                override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder): Float {
                    return 0.7f
                }

            }
            val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
            itemTouchHelper.attachToRecyclerView(recyclerViewCards)

            recyclerViewCards.adapter = cardsAdapter
        }
    }

    private fun setOnClickListeners() {
        binding.apply {
            floatingButtonAdd.setOnClickListener {
                viewModel.onFloatingButtonAddClicked()
            }
        }
    }

    private fun collectCardAndPlaybackInfo() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.cardAndPlaybackInfo.collectLatest {
                cardsAdapter.updatePlaybackInfo(it)
            }
        }
    }

    private fun collectListItems() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.listItems.collectLatest {
                cardsAdapter.submitListData(it)
            }
        }
    }

    private fun collectEvents() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.events.collect { event ->
                when (event) {
                    VocabularyEvent.NavigateToAddVocabularyCardFragment -> {
                        val action =
                            VocabularyFragmentDirections.actionNavigationVocabularyToAddEditVocabularyCardFragment(
                                null
                            )
                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                    }
                    is VocabularyEvent.NavigateToEditVocabularyCardFragment -> {
                        val action =
                            VocabularyFragmentDirections.actionNavigationVocabularyToAddEditVocabularyCardFragment(
                                event.card
                            )
                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                    }
                    is VocabularyEvent.ShowToast -> {
                        showToast(event.stringRes)
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_manage, menu)

        val searchView = menu.findItem(R.id.item_search).actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.onSearchQueryChanged(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.onSearchQueryChanged(newText)
                return true
            }
        })

        if (!viewModel.searchQuery.value.isNullOrBlank()) {
            searchView.setQuery(viewModel.searchQuery.value, false)
            searchView.isIconified = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.item_settings -> {
            val action = VocabularyFragmentDirections.actionNavigationVocabularyToSettingsFragment()
            navigateIfInDestination(CURRENT_DESTINATION_ID, action)
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCardClicked(card: VocabularyCard) {
        viewModel.onCardClicked(card)
    }

    override fun onPlayPauseClicked(card: VocabularyCard, fileName: String) {
        viewModel.onPlayPauseClicked(card, fileName)
    }

}