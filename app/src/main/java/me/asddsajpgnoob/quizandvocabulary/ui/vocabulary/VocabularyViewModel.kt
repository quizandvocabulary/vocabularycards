package me.asddsajpgnoob.quizandvocabulary.ui.vocabulary

import android.media.MediaPlayer
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabulary.data.model.room.VocabularyCardEntity.Companion.toDomainList
import me.asddsajpgnoob.quizandvocabulary.data.repository.InterfaceSettingsRepository
import me.asddsajpgnoob.quizandvocabulary.data.repository.VocabularyCardRepository
import me.asddsajpgnoob.quizandvocabulary.eventBus.EventBus
import me.asddsajpgnoob.quizandvocabulary.eventBus.model.VocabularyCardEvent
import me.asddsajpgnoob.quizandvocabulary.ui.common.model.PlaybackInfo
import me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.model.CardAndPlaybackInfo
import me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.model.VocabularyEvent
import me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.model.VocabularyListData
import me.asddsajpgnoob.quizandvocabulary.util.AudioPlayerFactory
import me.asddsajpgnoob.quizandvocabulary.util.Constants
import java.io.File

class VocabularyViewModel(
    interfaceSettingsRepository: InterfaceSettingsRepository,
    private val vocabularyCardRepository: VocabularyCardRepository,
    private val externalFilesDir: File,
    private val listInitialLoadSize: Int,
    private val listItemsPageSize: Int,
    private val listItemsMaxSize: Int
) : ViewModel() {

    private val _searchQuery = MutableStateFlow<String?>(null)
    val searchQuery = _searchQuery.asStateFlow()

    private val _searchedVocabularyCards = MutableStateFlow<List<VocabularyCard>>(listOf())

    private val _staticVocabularyCards = MutableStateFlow<List<VocabularyCard>>(listOf())

    private val showAudiosFlow = interfaceSettingsRepository.showAudiosInVocabularyScreen

    private val showExamplesFlow = interfaceSettingsRepository.showExamplesInVocabularyScreen

    val listItems: Flow<VocabularyListData> =
        combine(
            _searchQuery,
            _searchedVocabularyCards,
            _staticVocabularyCards,
            showAudiosFlow,
            showExamplesFlow
        ) { query, searchedCards, staticCards, showAudios, showExamples ->
            val items: List<VocabularyCard>
            val highlight: String?
            if (query.isNullOrBlank()) {
                items = staticCards
                highlight = null
            } else {
                items = searchedCards
                highlight = query
            }
            VocabularyListData(
                items,
                showAudios,
                showExamples,
                highlight
            )
        }

    private val _cardAndPlaybackInfo = MutableStateFlow<CardAndPlaybackInfo?>(null)
    val cardAndPlaybackInfo = _cardAndPlaybackInfo.asStateFlow()

    private val eventsChannel = Channel<VocabularyEvent>()
    val events = eventsChannel.receiveAsFlow()

    private val modifyStaticItemsMutex = Mutex()
    private val modifySearchedItemsMutex = Mutex()

    private var mediaPlayerPositionJob: Job? = null
    private var mediaPlayer: MediaPlayer? = null

    private var staticCardsShouldLoadOffset = 0
    private var searchedCardsShouldLoadOffset = 0

    private var loadNextStaticCardsJob: Job
    private var loadPrevStaticCardsJob: Job? = null
    private var staticCardsEndOfPaginationReached = false

    private var loadNextSearchedCardsJob: Job? = null
    private var loadPrevSearchedCardsJob: Job? = null
    private var searchedCardsEndOfPaginationReached = false

    init {
        loadNextStaticCardsJob = loadNextLocalVocabularyCards(true)
        viewModelScope.launch {
            EventBus.vocabularyCardEvent.collectLatest { event ->
                when (event) {
                    is VocabularyCardEvent.CardCreated -> {
                        onCardCreated(event.card)
                    }
                    is VocabularyCardEvent.CardDeleted -> {
                        onCardDeleted(event.card)
                    }
                    is VocabularyCardEvent.CardEdited -> {
                        onCardEdited(event.editedCard)
                    }
                }
            }
        }
    }

    fun onListBottomReached() {
        loadNextCards()
    }

    fun onListTopReached() {
        loadPrevCards()
    }

    fun onItemSwiped(swipedCard: VocabularyCard) {
        deleteCardFromDb(swipedCard)
        val query = _searchQuery.value
        if (!query.isNullOrBlank()) {
            viewModelScope.launch {
                modifySearchedListData {
                    val searchedResult = _searchedVocabularyCards.value.toMutableList()
                    val searchedIndex = searchedResult.indexOfFirst {
                        it.id == swipedCard.id
                    }
                    if (searchedIndex != -1) {
                        searchedResult.removeAt(searchedIndex)
                        _searchedVocabularyCards.value = searchedResult
                        searchedCardsShouldLoadOffset--
                    }
                }
            }
        }

        viewModelScope.launch {
            modifyStaticListData {
                val staticResult = _staticVocabularyCards.value.toMutableList()
                val index = staticResult.indexOfFirst {
                    it.id == swipedCard.id
                }
                if (index != -1) {
                    staticResult.removeAt(index)
                    _staticVocabularyCards.value = staticResult
                    staticCardsShouldLoadOffset--
                }
            }
        }
    }

    fun onSearchQueryChanged(query: String?) {
        if (_searchQuery.value == query) {
            return
        }
        _searchQuery.value = query
        loadNextSearchedCardsJob?.cancel()

        if (query.isNullOrBlank()) {
            return
        }
        searchedCardsEndOfPaginationReached = false
        searchedCardsShouldLoadOffset = 0
        loadNextSearchedCardsJob = loadNextLocalSearchedVocabularyCards(true)
    }

    fun onFloatingButtonAddClicked() = viewModelScope.launch {
        eventsChannel.send(VocabularyEvent.NavigateToAddVocabularyCardFragment)
    }

    fun onCardClicked(card: VocabularyCard) = viewModelScope.launch {
        eventsChannel.send(VocabularyEvent.NavigateToEditVocabularyCardFragment(card))
    }

    fun onPlayPauseClicked(card: VocabularyCard, fileName: String) {
        val playbackInfo = _cardAndPlaybackInfo.value
        if (playbackInfo != null && playbackInfo.playbackInfo.fileName != fileName) {
            releaseMediaPlayer()
        }
        if (!isMediaPlayerPlaying()) {
            if (mediaPlayer == null) {
                val audioFile =
                    File("${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}/${fileName}")
                if (!audioFile.exists()) {
                    showToast(R.string.Audio_file_not_found)
                    return
                }
                initMediaPlayer(
                    card,
                    audioFile
                )
            }
            startMediaPlayer()
            return
        }
        pauseMediaPlayer()
    }

    private fun showToast(@StringRes stringRes: Int) = viewModelScope.launch {
        eventsChannel.send(VocabularyEvent.ShowToast(stringRes))
    }

    private fun deleteCardFromDb(card: VocabularyCard) = viewModelScope.launch {
        vocabularyCardRepository.delete(card.toEntity())
        EventBus.submitVocabularyCardEvent(VocabularyCardEvent.CardDeleted(card))
    }

    private fun isMediaPlayerPlaying() = mediaPlayer?.isPlaying == true

    private fun initMediaPlayer(card: VocabularyCard, audioFile: File) {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer = AudioPlayerFactory.newInstance(audioFile)
        mediaPlayer!!.prepare()
        _cardAndPlaybackInfo.value = CardAndPlaybackInfo(
            PlaybackInfo(
                false,
                audioFile.name,
                mediaPlayer!!.duration,
                0
            ),
            card
        )
        mediaPlayer!!.setOnCompletionListener {
            releaseMediaPlayer()
            _cardAndPlaybackInfo.value = null
        }
    }

    private fun startMediaPlayer() {
        mediaPlayerPositionJob = viewModelScope.launch {
            while (isActive) {
                _cardAndPlaybackInfo.value = _cardAndPlaybackInfo.value!!.copy(
                    playbackInfo = cardAndPlaybackInfo.value!!.playbackInfo.copy(
                        playing = true,
                        playingPosition = mediaPlayer!!.currentPosition
                    )
                )
                delay(20L)
            }
        }
        mediaPlayer!!.start()
    }

    private fun pauseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer!!.pause()
        _cardAndPlaybackInfo.value = _cardAndPlaybackInfo.value!!.copy(
            playbackInfo = _cardAndPlaybackInfo.value!!.playbackInfo.copy(
                playing = false
            )
        )
    }

    private fun releaseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        if (isMediaPlayerPlaying()) {
            mediaPlayer!!.stop()
        }
        mediaPlayer?.release()
        mediaPlayer = null
        _cardAndPlaybackInfo.value = null
    }

    private fun onCardCreated(addedCard: VocabularyCard) {
        val query = _searchQuery.value
        if (!query.isNullOrBlank() &&
            (addedCard.origin.contains(query, ignoreCase = true) || addedCard.translation.contains(
                query,
                ignoreCase = true
            ))
        ) {
            viewModelScope.launch {
                modifySearchedListData {
                    val searchedResult = _searchedVocabularyCards.value.toMutableList()
                    searchedResult.add(0, addedCard)
                    _searchedVocabularyCards.value = searchedResult
                    searchedCardsShouldLoadOffset++
                }
            }
        }

        viewModelScope.launch {
            modifyStaticListData {
                val staticResult = _staticVocabularyCards.value.toMutableList()
                staticResult.add(0, addedCard)
                _staticVocabularyCards.value = staticResult
                staticCardsShouldLoadOffset++
            }
        }
    }

    private fun onCardEdited(editedCard: VocabularyCard) {
        val query = _searchQuery.value
        if (!query.isNullOrBlank()) {
            viewModelScope.launch {
                modifySearchedListData {
                    val searchedResult = _searchedVocabularyCards.value.toMutableList()
                    val searchedIndex = searchedResult.indexOfFirst {
                        it.id == editedCard.id
                    }
                    if (searchedIndex != -1) {
                        searchedResult[searchedIndex] = editedCard
                        _searchedVocabularyCards.value = searchedResult
                    }
                }
            }
        }

        viewModelScope.launch {
            modifyStaticListData {
                val staticResult = _staticVocabularyCards.value.toMutableList()
                val staticIndex = staticResult.indexOfFirst {
                    it.id == editedCard.id
                }
                if (staticIndex != -1) {
                    staticResult[staticIndex] = editedCard
                    _staticVocabularyCards.value = staticResult
                }
            }
        }
    }

    private fun onCardDeleted(deletedCard: VocabularyCard) {
        val query = _searchQuery.value
        if (!query.isNullOrBlank()) {
            viewModelScope.launch {
                modifySearchedListData {
                    val searchedResult = _searchedVocabularyCards.value.toMutableList()
                    val searchedIndex = searchedResult.indexOfFirst {
                        it.id == deletedCard.id
                    }
                    if (searchedIndex != -1) {
                        searchedResult.removeAt(searchedIndex)
                        _searchedVocabularyCards.value = searchedResult
                        searchedCardsShouldLoadOffset--
                    }
                }
            }
        }

        viewModelScope.launch {
            modifyStaticListData {
                val staticResult = _staticVocabularyCards.value.toMutableList()
                val staticIndex = staticResult.indexOfFirst {
                    it.id == deletedCard.id
                }
                if (staticIndex != -1) {
                    staticResult.removeAt(staticIndex)
                    _staticVocabularyCards.value = staticResult
                    staticCardsShouldLoadOffset--
                }
            }
        }
    }

    private fun loadPrevCards() {
        if (_searchQuery.value.isNullOrBlank()) {
            if (
                (loadPrevStaticCardsJob == null || loadPrevStaticCardsJob!!.isCompleted) &&
                staticCardsShouldLoadOffset > listItemsMaxSize
            ) {
                loadPrevStaticCardsJob = loadPrevLocalVocabularyCards()
            }
            return
        }
        if (
            (loadPrevSearchedCardsJob == null || loadPrevSearchedCardsJob!!.isCompleted) &&
            searchedCardsShouldLoadOffset > listItemsMaxSize
        ) {
            loadPrevSearchedCardsJob = loadPrevLocalSearchedVocabularyCards()
        }
    }

    private fun loadNextCards() {
        if (_searchQuery.value.isNullOrBlank()) {
            if (loadNextStaticCardsJob.isCompleted &&
                !staticCardsEndOfPaginationReached
            ) {
                loadNextStaticCardsJob = loadNextLocalVocabularyCards(false)
            }
            return
        }
        if (
            (loadNextSearchedCardsJob == null || loadNextSearchedCardsJob!!.isCompleted) &&
            !searchedCardsEndOfPaginationReached
        ) {
            loadNextSearchedCardsJob = loadNextLocalSearchedVocabularyCards(false)
        }
    }

    private fun loadNextLocalVocabularyCards(refresh: Boolean) = viewModelScope.launch {
        loadPrevStaticCardsJob?.join()
        modifyStaticListData {
            val limit = if (refresh) {
                listInitialLoadSize
            } else {
                listItemsPageSize
            }
            val entities = vocabularyCardRepository.findNewestVocabularyCards(
                staticCardsShouldLoadOffset,
                limit
            )

            var drop: List<VocabularyCard>
            withContext(Dispatchers.Default) {
                val result = if (refresh) {
                    mutableListOf()
                } else {
                    _staticVocabularyCards.value.toMutableList()
                }
                result.addAll(entities.toDomainList())
                drop = result
                val diff = result.size - listItemsMaxSize
                if (diff > 0) {
                    drop = drop.drop(diff)
                }
            }

            if (entities.size < limit) {
                staticCardsEndOfPaginationReached = true
            }
            _staticVocabularyCards.value = drop
            staticCardsShouldLoadOffset += entities.size
        }
    }

    private fun loadNextLocalSearchedVocabularyCards(refresh: Boolean) = viewModelScope.launch {
        loadPrevSearchedCardsJob?.join()
        modifySearchedListData {
            val limit = if (refresh) {
                listInitialLoadSize
            } else {
                listItemsPageSize
            }
            val entities =
                vocabularyCardRepository.searchVocabularyCardsByOriginOrTranslationOrderByCreatedAtDesc(
                    _searchQuery.value!!,
                    searchedCardsShouldLoadOffset,
                    limit
                )

            var drop: List<VocabularyCard>
            withContext(Dispatchers.Default) {
                val result = if (refresh) {
                    mutableListOf()
                } else {
                    _searchedVocabularyCards.value.toMutableList()
                }
                result.addAll(entities.toDomainList())
                drop = result
                val diff = result.size - listItemsMaxSize
                if (diff > 0) {
                    drop = drop.drop(diff)
                }
            }

            if (entities.size < limit) {
                searchedCardsEndOfPaginationReached = true
            }
            _searchedVocabularyCards.value = drop
            searchedCardsShouldLoadOffset += entities.size
        }
    }

    private fun loadPrevLocalVocabularyCards() = viewModelScope.launch {
        loadNextStaticCardsJob.join()
        modifyStaticListData {
            var offset = staticCardsShouldLoadOffset - listItemsMaxSize - listItemsPageSize
            var limit = staticCardsShouldLoadOffset % listItemsPageSize
            if (limit == 0) {
                limit = listItemsPageSize
            }
            if (offset < 0) {
                offset = 0
            }
            val entities = vocabularyCardRepository.findNewestVocabularyCards(
                offset,
                limit
            )

            var drop: List<VocabularyCard>
            withContext(Dispatchers.Default) {
                val result = mutableListOf<VocabularyCard>()
                result.addAll(entities.toDomainList())
                result.addAll(_staticVocabularyCards.value)
                drop = result
                val diff = result.size - listItemsMaxSize
                if (diff > 0) {
                    drop = drop.dropLast(diff)
                }
            }

            if (entities.isNotEmpty()) {
                staticCardsEndOfPaginationReached = false
            }
            _staticVocabularyCards.value = drop
            staticCardsShouldLoadOffset -= entities.size
        }
    }

    private fun loadPrevLocalSearchedVocabularyCards() = viewModelScope.launch {
        loadNextSearchedCardsJob?.join()
        modifySearchedListData {
            var offset = searchedCardsShouldLoadOffset - listItemsMaxSize - listItemsPageSize
            var limit = searchedCardsShouldLoadOffset % listItemsPageSize
            if (limit == 0) {
                limit = listItemsPageSize
            }
            if (offset < 0) {
                offset = 0
            }
            val entities =
                vocabularyCardRepository.searchVocabularyCardsByOriginOrTranslationOrderByCreatedAtDesc(
                    _searchQuery.value!!,
                    offset,
                    limit
                )

            var drop: List<VocabularyCard>
            withContext(Dispatchers.Default) {
                val result = mutableListOf<VocabularyCard>()
                result.addAll(entities.toDomainList())
                result.addAll(_searchedVocabularyCards.value.toMutableList())
                drop = result
                val diff = result.size - listItemsMaxSize
                if (diff > 0) {
                    drop = result.dropLast(diff)
                }
            }

            if (entities.isNotEmpty()) {
                searchedCardsEndOfPaginationReached = false
            }
            _searchedVocabularyCards.value = drop
            searchedCardsShouldLoadOffset -= entities.size
        }
    }

    private suspend inline fun modifyStaticListData(action: () -> Unit) =
        modifyStaticItemsMutex.withLock {
            action.invoke()
        }

    private suspend inline fun modifySearchedListData(action: () -> Unit) =
        modifySearchedItemsMutex.withLock {
            action.invoke()
        }

    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()
    }

    class Factory(
        private val interfaceSettingsRepository: InterfaceSettingsRepository,
        private val vocabularyCardRepository: VocabularyCardRepository,
        private val externalFilesDir: File,
        private val listInitialLoadSize: Int,
        private val listItemsPageSize: Int,
        private val listItemsMaxSize: Int
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(VocabularyViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return VocabularyViewModel(
                    interfaceSettingsRepository,
                    vocabularyCardRepository,
                    externalFilesDir,
                    listInitialLoadSize,
                    listItemsPageSize,
                    listItemsMaxSize
                ) as T
            } else {
                throw IllegalArgumentException("Unable to create ${VocabularyViewModel::class.qualifiedName}")
            }
        }
    }
}