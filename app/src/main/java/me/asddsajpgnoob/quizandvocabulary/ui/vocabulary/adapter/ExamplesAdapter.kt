package me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import me.asddsajpgnoob.quizandvocabulary.databinding.ItemExampleBinding
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseViewHolder

class ExamplesAdapter : RecyclerView.Adapter<ExamplesAdapter.ExampleViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return true
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    private val items get() = differ.currentList

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ExampleViewHolder {
        val binding =
            ItemExampleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ExampleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) =
        holder.bind(items[position])

    override fun getItemCount() = items.size

    fun submitListData(newList: List<String>) {
        differ.submitList(newList)
    }

    inner class ExampleViewHolder(binding: ItemExampleBinding) :
        BaseViewHolder<String, ItemExampleBinding>(binding) {

        override fun bind(item: String) {
            binding.editTextExample.text = item
        }
    }
}