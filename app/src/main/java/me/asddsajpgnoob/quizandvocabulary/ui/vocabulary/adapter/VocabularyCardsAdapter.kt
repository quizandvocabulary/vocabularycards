package me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabulary.databinding.ItemVocabularyCardBinding
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseViewHolder
import me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.model.CardAndPlaybackInfo
import me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.model.VocabularyListData
import me.asddsajpgnoob.quizandvocabulary.util.TouchUtils
import me.asddsajpgnoob.quizandvocabulary.util.highlightSubstring

class VocabularyCardsAdapter(
    private val connector: VocabularyCardAdapterConnector
) : RecyclerView.Adapter<VocabularyCardsAdapter.VocabularyCardViewHolder>() {

    companion object {
        private const val PAYLOAD_SEARCH_HIGHLIGHT = '0'
    }

    private var highlight: String? = null

    private var showAudios: Boolean = false
    private var showExamples: Boolean = false

    private var currentPlaybackInfo: CardAndPlaybackInfo? = null
    private var playingCardHolder: VocabularyCardViewHolder? = null

    private val items = mutableListOf<VocabularyCard>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VocabularyCardViewHolder {
        val binding =
            ItemVocabularyCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VocabularyCardViewHolder(binding)
    }

    override fun onBindViewHolder(holder: VocabularyCardViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onBindViewHolder(
        holder: VocabularyCardViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
            return
        }
        payloads.forEach {
            var payloadsHandled = false
            if ((it as String).contains(PAYLOAD_SEARCH_HIGHLIGHT)) {
                payloadsHandled = true
                holder.setupOriginAndTranslation(items[position])
            }
            if (!payloadsHandled) {
                super.onBindViewHolder(holder, position, payloads)
            }
        }
    }

    override fun getItemCount() = items.size

    override fun onViewRecycled(holder: VocabularyCardViewHolder) {
        super.onViewRecycled(holder)
        val pos = holder.adapterPosition
        if (pos != NO_POSITION) {
            val item = items[holder.adapterPosition]
            val playingInfo = currentPlaybackInfo
            if (item.id == playingInfo?.card?.id) {
                playingCardHolder?.updateAudiosAdapterPlaybackInfo(null)
                playingCardHolder = null
            }
        }
    }

    fun submitListData(data: VocabularyListData) {
        val oldItems = items.toList()
        val diffCallback = VocabularyCardsDiffCallback(
            oldList = oldItems,
            newList = data.items,
            oldShowAudios = showAudios,
            newShowAudios = data.showAudios,
            oldShowExamples = showExamples,
            newShowExamples = data.showExamples,
            oldHighlight = highlight,
            newHighlight = data.highlight
        )
        val diffResult = DiffUtil.calculateDiff(diffCallback, false)
        items.clear()
        items.addAll(data.items)
        showAudios = data.showAudios
        showExamples = data.showExamples
        highlight = data.highlight
        diffResult.dispatchUpdatesTo(this)
    }

    fun getItem(position: Int) = items[position]

    fun updatePlaybackInfo(newPlaybackInfo: CardAndPlaybackInfo?) {
        playingCardHolder?.updateAudiosAdapterPlaybackInfo(newPlaybackInfo)
        currentPlaybackInfo = newPlaybackInfo
    }

    @SuppressLint("ClickableViewAccessibility")
    inner class VocabularyCardViewHolder(binding: ItemVocabularyCardBinding) :
        BaseViewHolder<VocabularyCard, ItemVocabularyCardBinding>(binding),
        AudiosAdapter.AudiosAdapterListener {

        private val highlightColor =
            ContextCompat.getColor(binding.root.context, R.color.search_result_highlighted_text)

        init {
            binding.apply {
                recyclerViewExamples.setHasFixedSize(true)
                recyclerViewAudios.setHasFixedSize(true)

                root.setOnClickListener {
                    val pos = adapterPosition
                    if (pos != NO_POSITION) {
                        val card = items[pos]
                        connector.onCardClicked(card)
                    }
                }
                recyclerViewExamples.setOnTouchListener(object : View.OnTouchListener {
                    private var touchCanBeClick = false

                    private var startX = 0f
                    private var startY = 0f

                    override fun onTouch(v: View, event: MotionEvent): Boolean {
                        when (event.action) {
                            MotionEvent.ACTION_DOWN -> {
                                startX = event.x
                                startY = event.y
                                touchCanBeClick = true

                                root.isSelected = true
                                return true
                            }
                            MotionEvent.ACTION_UP -> {
                                if (touchCanBeClick) {
                                    root.performClick()
                                }

                                touchCanBeClick = false
                                startX = 0f
                                startY = 0f
                                root.isSelected = false

                                return true
                            }
                            MotionEvent.ACTION_CANCEL -> {
                                root.isSelected = false
                                startX = 0f
                                startY = 0f
                                touchCanBeClick = false
                                return true
                            }
                            MotionEvent.ACTION_MOVE -> {
                                val endX = event.x
                                val endY = event.y

                                if (touchCanBeClick) {
                                    if (!TouchUtils.isInClickArea(startX, endX, startY, endY)) {
                                        startX = 0f
                                        startY = 0f
                                        touchCanBeClick = false
                                        root.isSelected = false
                                    }
                                }
                                return false
                            }
                            else -> {
                                return false
                            }
                        }
                    }
                })

                recyclerViewAudios.setOnTouchListener(object : View.OnTouchListener {
                    private var touchCanBeClick = false

                    private var startX = 0f
                    private var startY = 0f

                    override fun onTouch(v: View?, event: MotionEvent): Boolean {
                        when (event.action) {
                            MotionEvent.ACTION_DOWN -> {
                                startX = event.x
                                startY = event.y
                                touchCanBeClick = true

                                root.isSelected = true
                                return true
                            }
                            MotionEvent.ACTION_UP -> {
                                if (touchCanBeClick) {
                                    root.performClick()
                                }

                                touchCanBeClick = false
                                startX = 0f
                                startY = 0f
                                root.isSelected = false

                                return true
                            }
                            MotionEvent.ACTION_CANCEL -> {
                                root.isSelected = false
                                startX = 0f
                                startY = 0f
                                touchCanBeClick = false
                                return true
                            }
                            MotionEvent.ACTION_MOVE -> {
                                val endX = event.x
                                val endY = event.y

                                if (touchCanBeClick) {
                                    if (!TouchUtils.isInClickArea(startX, endX, startY, endY)) {
                                        startX = 0f
                                        startY = 0f
                                        touchCanBeClick = false
                                        root.isSelected = false
                                    }
                                }
                                return false
                            }
                            else -> {
                                return false
                            }
                        }
                    }

                })
            }
        }

        override fun bind(item: VocabularyCard) {
            val playingCardPlaybackInfo = currentPlaybackInfo

            setupOriginAndTranslation(item)

            binding.apply {
                if (showExamples && item.examples.isNotEmpty()) {
                    recyclerViewExamples.isVisible = true
                    val examplesAdapter = ExamplesAdapter()
                    recyclerViewExamples.adapter = examplesAdapter
                    examplesAdapter.submitListData(item.examples)
                } else {
                    recyclerViewExamples.adapter = null
                    recyclerViewExamples.isVisible = false
                }

                if (showAudios && item.audioFiles.isNotEmpty()) {
                    recyclerViewAudios.isVisible = true
                    val audiosAdapter = AudiosAdapter(this@VocabularyCardViewHolder)
                    recyclerViewAudios.adapter = audiosAdapter
                    audiosAdapter.submitListData(playingCardPlaybackInfo, item.audioFiles)
                } else {
                    recyclerViewAudios.adapter = null
                    recyclerViewAudios.isVisible = false
                }
            }

            val playing = item.id == playingCardPlaybackInfo?.card?.id
            if (playing) {
                playingCardHolder = this
                playingCardHolder
                updateAudiosAdapterPlaybackInfo(currentPlaybackInfo)
            } else {
                updateAudiosAdapterPlaybackInfo(null)
            }
        }

        fun setupOriginAndTranslation(item: VocabularyCard) {
            binding.apply {
                if (highlight != null) {
                    textOrigin.text = item.origin.highlightSubstring(
                        highlight!!,
                        highlightColor,
                        ignoreCase = true
                    )
                    textTranslation.text = item.translation.highlightSubstring(
                        highlight!!,
                        highlightColor,
                        ignoreCase = true
                    )
                } else {
                    textOrigin.text = item.origin
                    textTranslation.text = item.translation
                }
            }
        }

        override fun onPlayPauseClicked(fileName: String) {
            val pos = adapterPosition
            if (pos != NO_POSITION) {
                playingCardHolder?.updateAudiosAdapterPlaybackInfo(null)
                playingCardHolder = this
                connector.onPlayPauseClicked(items[pos], fileName)
            }
        }

        fun updateAudiosAdapterPlaybackInfo(newCardAndPlaybackInfo: CardAndPlaybackInfo?) {
            val adapter = binding.recyclerViewAudios.adapter as? AudiosAdapter
            adapter?.updatePlaybackInfo(newCardAndPlaybackInfo)
        }
    }

    class VocabularyCardsDiffCallback(
        private val oldList: List<VocabularyCard>,
        private val newList: List<VocabularyCard>,
        private val oldShowAudios: Boolean,
        private val newShowAudios: Boolean,
        private val oldShowExamples: Boolean,
        private val newShowExamples: Boolean,
        private val oldHighlight: String?,
        private val newHighlight: String?
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int {
            return oldList.size
        }

        override fun getNewListSize(): Int {
            return newList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }


        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            if (oldShowAudios != newShowAudios) {
                return false
            }
            if (oldShowExamples != newShowExamples) {
                return false
            }
            if (oldHighlight != newHighlight) {
                return false
            }
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            if (oldShowAudios != newShowAudios) {
                return null
            }
            if (oldShowExamples != newShowExamples) {
                return null
            }
            if (oldList[oldItemPosition] != newList[newItemPosition]) {
                return null
            }
            val result = buildString {
                if (oldHighlight != newHighlight) {
                    append(PAYLOAD_SEARCH_HIGHLIGHT)
                }
            }
            return result.ifEmpty {
                null
            }
        }
    }

    interface VocabularyCardAdapterConnector {
        fun onCardClicked(card: VocabularyCard)

        fun onPlayPauseClicked(card: VocabularyCard, fileName: String)
    }
}