package me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.model

import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabulary.ui.common.model.PlaybackInfo

data class CardAndPlaybackInfo(
    val playbackInfo: PlaybackInfo,
    val card: VocabularyCard
)