package me.asddsajpgnoob.quizandvocabulary.ui.vocabulary.model

import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard

data class VocabularyListData(
    val items: List<VocabularyCard>,
    val showAudios: Boolean,
    val showExamples: Boolean,
    val highlight: String?
)