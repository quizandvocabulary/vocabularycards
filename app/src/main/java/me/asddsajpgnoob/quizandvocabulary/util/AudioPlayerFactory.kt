package me.asddsajpgnoob.quizandvocabulary.util

import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import java.io.File
import java.io.FileInputStream

object AudioPlayerFactory {

    fun newInstance(file: File): MediaPlayer {
        return MediaPlayer().apply {
            setAudioAttributes(
                AudioAttributes.Builder()
                    .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                    .setLegacyStreamType(AudioManager.STREAM_VOICE_CALL)
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build()
            )
            setScreenOnWhilePlaying(true)
            isLooping = false
            val inputStream = FileInputStream(file)
            setDataSource(inputStream.fd)
            inputStream.close()
        }
    }

    fun newInstance(url: String): MediaPlayer {
        return MediaPlayer().apply {
            AudioAttributes.Builder()
                .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                .setLegacyStreamType(AudioManager.STREAM_VOICE_CALL)
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build()
            setScreenOnWhilePlaying(true)
            isLooping = false
            setDataSource(url)
        }
    }
}