package me.asddsajpgnoob.quizandvocabulary.util

import android.content.Context
import android.media.MediaRecorder
import android.os.Build
import kotlinx.coroutines.*
import me.asddsajpgnoob.quizandvocabulary.App
import java.io.File
import java.util.*

class AudioRecorder private constructor(
    private val context: Context,
    private val audioSource: Int,
    private val outputFormat: Int,
    private val audioEncoder: Int,
    private val fileNameGenerator: () -> String,
    private val coroutineScope: CoroutineScope,
    private var saveToDirectory: File
) {

    companion object {
        const val RECORDING_TIME_NO_LIMIT = 0L
    }

    private var recordingTime = 0L

    var isRecording: Boolean = false
        private set

    var outputPath: String? = null
        private set

    private var timerJob: Job? = null

    private var onRecordingTimeChangesListener: ((milliseconds: Long) -> Unit)? = null

    private var mediaRecorder: MediaRecorder? = null

    fun removeLastOutput() {
        outputPath?.let {
            File(it).delete()
        }
        outputPath = null
    }

    fun setOnRecordingTimeChangesListener(listener: (milliseconds: Long) -> Unit) {
        this.onRecordingTimeChangesListener = listener
    }

    fun startRecording(
        recordingTimeLimit: Long = RECORDING_TIME_NO_LIMIT,
        onLimitReachedListener: (() -> Unit)? = null
    ) {
        if (isRecording) {
            throw IllegalStateException("Already recording")
        }
        recordingTime = 0L
        init()
        mediaRecorder!!.apply {
            prepare()
            start()
        }
        isRecording = true
        timerJob = coroutineScope.launch(Dispatchers.Default) {
            while (isActive) {
                onRecordingTimeChangesListener?.invoke(recordingTime)
                if (recordingTimeLimit in (RECORDING_TIME_NO_LIMIT + 1)..recordingTime) {
                    stopRecording()
                    onLimitReachedListener?.invoke()
                }
                delay(100L)
                recordingTime += 100L
            }
        }
    }

    @Throws(RuntimeException::class)
    fun stopRecording() {
        if (!isRecording) {
            throw IllegalStateException("Not recording")
        }
        timerJob?.cancel()
        timerJob = null
        recordingTime = 0L
        mediaRecorder!!.apply {
            stop()
            release()
        }
        isRecording = false
    }

    private fun init() {
        mediaRecorder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            MediaRecorder(context)
        } else {
            @Suppress("deprecation")
            MediaRecorder()
        }

        mediaRecorder!!.apply {
            setAudioSource(audioSource)
            setOutputFormat(outputFormat)
            setAudioEncoder(audioEncoder)
        }

        val fileName = fileNameGenerator.invoke()

        outputPath = "${saveToDirectory.absolutePath}/$fileName.aac"

        mediaRecorder!!.setOutputFile(outputPath!!)
    }

    class Builder {
        private var audioSource: Int = MediaRecorder.AudioSource.MIC
        private var outputFormat: Int = MediaRecorder.OutputFormat.AAC_ADTS
        private var audioEncoder: Int = MediaRecorder.AudioEncoder.AAC
        private var coroutineScope: CoroutineScope = App.INSTANCE.appScope
        private var saveToDirectory: File = App.INSTANCE.cacheDir
        private var fileNameGenerator: () -> String = {
            DateUtils.dateToString(Date(), pattern = DateUtils.FILE_NAME_FORMAT)
        }

        fun setAudioSource(audioSource: Int): Builder = apply {
            this.audioSource = audioSource
        }

        fun setOutputFormat(outputFormat: Int): Builder = apply {
            this.outputFormat = outputFormat
        }

        fun setAudioEncoder(audioEncoder: Int): Builder = apply {
            this.audioEncoder = audioEncoder
        }

        fun setCoroutineScope(coroutineScope: CoroutineScope): Builder = apply {
            this.coroutineScope = coroutineScope
        }

        fun saveRecordingsToDirectory(saveToDirectory: File): Builder = apply {
            this.saveToDirectory = saveToDirectory
        }

        fun setFileNameGenerator(fileNameGenerator: () -> String): Builder = apply {
            this.fileNameGenerator = fileNameGenerator
        }

        fun build() = AudioRecorder(
            App.INSTANCE,
            audioSource,
            outputFormat,
            audioEncoder,
            fileNameGenerator,
            coroutineScope,
            saveToDirectory
        )
    }

}