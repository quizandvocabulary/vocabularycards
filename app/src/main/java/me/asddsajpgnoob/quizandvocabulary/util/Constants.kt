package me.asddsajpgnoob.quizandvocabulary.util

object Constants {

    const val AUDIOS_DIRECTORY_NAME = "recorded_audios"

    const val ADD_EDIT_CARD_SHOW_IN_QUIZ_INITIAL_VALUE = true

    const val GOOGLE_TRANSLATE_URL_UNFORMATTED = "https://translate.google.com?sl=auto&text=%s&op=translate"
    const val GOOGLE_TRANSLATE_APP_URL = "https://translate.google.com/"

    const val MIN_DEVICE_HEIGHT_TO_SHOW_QUIZ_ADDITIONAL_DATA_DP = 1100
    const val QUIZ_NORMAL_DATA_EXAMPLES_COUNT = 3
    const val QUIZ_ADDITIONAL_DATA_EXAMPLES_COUNT = 5
    const val QUIZ_NORMAL_DATA_AUDIOS_COUNT = 1
    const val QUIZ_ADDITIONAL_DATA_AUDIOS_COUNT = 2
}