package me.asddsajpgnoob.quizandvocabulary.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    const val DB_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"

    const val FILE_NAME_FORMAT = "yyyy-MM-dd_HH-mm-ss"

    fun stringToDate(
        source: String,
        pattern: String = DB_DATE_TIME_FORMAT,
        timeZone: TimeZone? = null,
        locale: Locale = Locale.getDefault()
    ): Date? {
        val sourceForJava = source.replace("Z", "+0000")
        val dateFormat = SimpleDateFormat(pattern, locale)
        timeZone?.let {
            dateFormat.timeZone = it
        }
        return try {
            dateFormat.parse(sourceForJava)
        } catch (e: ParseException) {
            null
        }
    }

    fun dateToString(
        date: Date,
        pattern: String = DB_DATE_TIME_FORMAT,
        timeZone: TimeZone? = null,
        locale: Locale = Locale.getDefault()
    ): String {
        val dateFormat = SimpleDateFormat(pattern, locale)
        timeZone?.let {
            dateFormat.timeZone = it
        }
        return dateFormat.format(date)
    }
}