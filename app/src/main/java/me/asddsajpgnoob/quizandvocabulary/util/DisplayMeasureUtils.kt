package me.asddsajpgnoob.quizandvocabulary.util

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.WindowManager


object DisplayMeasureUtils {

//    fun getStatusBarHeight(resources: Resources): Int {
//        val statusBarHeightId = resources.getIdentifier("status_bar_height", "dimen", "android")
//
//        return resources.getDimensionPixelSize(statusBarHeightId)
//    }

    /**
     * @return Pair<Int, Int> first is height second is width
     * */
    fun getDeviceSize(context: Context): Pair<Int, Int> {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val bounds = windowManager.currentWindowMetrics.bounds
            bounds.right to bounds.bottom
        } else {
            val displayMetrics = DisplayMetrics()
            @Suppress("deprecation")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            val height = displayMetrics.heightPixels
            val width = displayMetrics.widthPixels
            width to height
        }
    }

    fun getDeviceSizeDp(context: Context): Pair<Int, Int> {
        val size = getDeviceSize(context)
        return Pair(pxToDp(context, size.first), pxToDp(context, size.second))
    }

    fun dpToPx(context: Context, dp: Float): Float {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics)
    }

    fun dpToPx(context: Context, dp: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), displayMetrics).toInt()
    }

    fun pxToDp(context: Context, px: Float): Float {
        return px / context.resources.displayMetrics.density
    }

    fun pxToDp(context: Context, px: Int): Int {
        return px / context.resources.displayMetrics.density.toInt()
    }
}