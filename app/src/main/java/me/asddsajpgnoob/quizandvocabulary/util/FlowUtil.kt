package me.asddsajpgnoob.quizandvocabulary.util

import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.emptyPreferences
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import java.io.IOException

fun Flow<Preferences>.emitEmptyIfIOException() = catch { e ->
    if (e is IOException) {
        emit(emptyPreferences())
    } else {
        throw e
    }
}