package me.asddsajpgnoob.quizandvocabulary.util

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.core.content.ContextCompat.startActivity
import me.asddsajpgnoob.quizandvocabulary.BuildConfig


object IntentUtils {

    fun getActionViewIntent(url: String): Intent {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        return intent
    }

    fun getAppSettingsIntent(): Intent {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
        intent.data = uri
        return intent
    }

    fun createChooser(intent: Intent, extraIntents: Array<Intent>, title: String): Intent {
        val chooser = Intent(Intent.ACTION_CHOOSER)
        chooser.putExtra(Intent.EXTRA_INTENT, intent)
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents)
        chooser.putExtra(Intent.EXTRA_TITLE, title)
        return chooser
    }
}

fun Intent.applyNewTask(): Intent = apply {
    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
}

fun Intent.applyNewTaskIf(condition: Boolean): Intent = apply {
    if (condition) {
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    }
}

fun Intent.applyNewTaskIf(condition: () -> Boolean): Intent = apply {
    if (condition.invoke()) {
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    }
}