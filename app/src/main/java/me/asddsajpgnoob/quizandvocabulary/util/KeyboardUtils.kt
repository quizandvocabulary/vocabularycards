package me.asddsajpgnoob.quizandvocabulary.util

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager

fun Activity.hideKeyboard(clearFocus: Boolean = false) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    val cf = currentFocus
    imm.hideSoftInputFromWindow(cf?.windowToken, 0)
    if (clearFocus) {
        cf?.clearFocus()
    }
}