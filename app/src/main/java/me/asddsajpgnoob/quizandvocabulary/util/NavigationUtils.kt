package me.asddsajpgnoob.quizandvocabulary.util

import android.os.Bundle
import androidx.core.view.iterator
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import me.asddsajpgnoob.quizandvocabulary.R
import java.lang.ref.WeakReference

fun BottomNavigationView.customSetupWithNavController(
    navController: NavController,
    getSaveNavigationState: () -> Boolean
) {
    setOnItemSelectedListener { item ->
        val saveState = getSaveNavigationState.invoke()
        val builder = NavOptions.Builder().setLaunchSingleTop(true).setRestoreState(saveState)
        builder.setEnterAnim(R.anim.nav_enter)
            .setExitAnim(R.anim.nav_exit)
            .setPopEnterAnim(R.anim.nav_enter)
            .setPopExitAnim(R.anim.nav_exit)
//        if (item.order and Menu.CATEGORY_SECONDARY == 0) {
        builder.setPopUpTo(
            navController.graph.findStartDestination().id,
            inclusive = false,
            saveState = saveState
        )
//        }
        val options = builder.build()
        try {
            navController.navigate(item.itemId, null, options)
            true
        } catch (e: IllegalArgumentException) {
            false
        }
    }

    val weakReference = WeakReference(this)

    navController.addOnDestinationChangedListener(object :
        NavController.OnDestinationChangedListener {

        override fun onDestinationChanged(
            controller: NavController,
            destination: NavDestination,
            arguments: Bundle?
        ) {
            val view = weakReference.get()
            if (view == null) {
                navController.removeOnDestinationChangedListener(this)
                return
            }
            val iterator = view.menu.iterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                val checked = destination.id == item.itemId
                if (checked) {
                    item.isChecked = checked
                    break
                }
            }
        }
    })
}