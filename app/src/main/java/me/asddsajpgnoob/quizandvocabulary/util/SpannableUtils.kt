package me.asddsajpgnoob.quizandvocabulary.util

import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan

fun String.highlight(start: Int, end: Int, color: Int): Spannable {
    val spannableString = SpannableString(this)
    spannableString.setSpan(BackgroundColorSpan(color), start, end, 0)
    return spannableString
}

fun String.highlightFirstSubstring(substring: String, color: Int, ignoreCase: Boolean = false): Spannable {
    val spannableString = SpannableString(this)
    val start: Int = indexOf(substring, ignoreCase = ignoreCase)
    if (start == -1) {
        return spannableString
    }
    val end: Int = start + substring.length
    spannableString.setSpan(BackgroundColorSpan(color), start, end, 0)
    return spannableString
}

fun String.highlightSubstring(substring: String, color: Int, ignoreCase: Boolean = false): Spannable {
    val spannableString = SpannableString(this)

    var start: Int = indexOf(substring, startIndex = 0)
    var end: Int = start + substring.length
    while (start != -1) {
        spannableString.setSpan(BackgroundColorSpan(color), start, end, 0)
        start = indexOf(substring, startIndex = end, ignoreCase = ignoreCase)
        end = start + substring.length
    }
    return spannableString
}