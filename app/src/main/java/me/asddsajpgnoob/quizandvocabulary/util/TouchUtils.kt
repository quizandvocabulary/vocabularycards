package me.asddsajpgnoob.quizandvocabulary.util

import kotlin.math.abs

object TouchUtils {

    fun isInClickArea(startX: Float, endX: Float, startY: Float, endY: Float, clickThreshold: Float = 500f): Boolean {
        val differenceX = abs(startX - endX)
        val differenceY = abs(startY - endY)
        return differenceX < clickThreshold && differenceY < clickThreshold
    }
}