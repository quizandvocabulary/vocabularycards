package me.asddsajpgnoob.quizandvocabulary.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import me.asddsajpgnoob.quizandvocabulary.R
import kotlin.math.min

class WaveView(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
    View(context, attrs, defStyleAttr, defStyleRes) {

    companion object {
        private const val FRAMES_PER_SECOND = 60L
    }

    var isAnimating = false

//    private val waveMatrix = Matrix()

    private var shouldResetWaves = false

    private var centerX: Int = 0
    private var centerY: Int = 0

    private var startTime: Long = System.currentTimeMillis()

    private val minDelay: Float

    private val paints: List<Paint>

    private val color: Int
    private val startRadius: Float
    private val endRadius: Float
    private val animationDuration: Int
    private val waveCount: Int

    init {
        val attrsTypedArray = context.obtainStyledAttributes(attrs, R.styleable.WaveView)
        color = attrsTypedArray.getColor(
            R.styleable.WaveView_android_color,
            ContextCompat.getColor(context, R.color.grey)
        )
        startRadius = attrsTypedArray.getDimension(R.styleable.WaveView_startRadius, 200f)
        endRadius = attrsTypedArray.getDimension(R.styleable.WaveView_endRadius, 400f)
        animationDuration = attrsTypedArray.getInt(R.styleable.WaveView_oneWaveDuration, 1000)
        waveCount = attrsTypedArray.getInt(R.styleable.WaveView_waveCount, 2)

        attrsTypedArray.recycle()

        val mutablePaints = mutableListOf<Paint>()
        for (i in 0 until waveCount) {
            val paint = Paint()
            paint.color = color
            mutablePaints.add(paint)
        }
        paints = mutablePaints

        minDelay = animationDuration.toFloat() / waveCount.toFloat()

        postInvalidate()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0
    )

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context) : this(context, null)

    fun resetWaves() {
        shouldResetWaves = true
        startTime = System.currentTimeMillis()
        postInvalidate()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val minSize = (endRadius * 2).toInt() + 1
        val desiredWidth = minSize + paddingStart + paddingEnd
        val desiredHeight = minSize + paddingTop + paddingBottom

        val width = measureDimension(desiredWidth, widthMeasureSpec, layoutParams.width)
        val height = measureDimension(desiredHeight, heightMeasureSpec, layoutParams.height)

        centerX = width / 2
        centerY = height / 2

        setMeasuredDimension(width, height)
    }

    private fun measureDimension(desiredSize: Int, measureSpec: Int, layoutParam: Int): Int {
        val specSize = MeasureSpec.getSize(measureSpec)
        if (layoutParam == ViewGroup.LayoutParams.MATCH_PARENT) {
            return specSize
        }

        val specMode = MeasureSpec.getMode(measureSpec)

        val result: Int = if (specMode == MeasureSpec.EXACTLY) {
            specSize
        } else {
            if (specMode == MeasureSpec.AT_MOST) {
                min(desiredSize, specSize)
            } else {
                desiredSize
            }
        }

        return result
    }

    override fun onDraw(canvas: Canvas) {
        if (shouldResetWaves) {
            paints[0].alpha = 255
            canvas.drawCircle(centerX.toFloat(), centerY.toFloat(), startRadius, paints[0])

            shouldResetWaves = false
            if (isAnimating) {
                postInvalidateDelayed(1000L / FRAMES_PER_SECOND)
            }
            return
        }

        val elapsedTime: Long = System.currentTimeMillis() - startTime

        val radiuses = mutableListOf<Float>()
        for (i in 0 until waveCount) {
            val delay: Float = minDelay * i.toFloat()

            val waveElapsedTime = if (elapsedTime < delay) {
                0f
            } else {
                elapsedTime.toFloat() - delay
            }
            val coefficient =
                waveElapsedTime % animationDuration.toFloat() / animationDuration.toFloat()

            paints[i].alpha = (255f - coefficient * 255f).toInt()

            val radius = ((endRadius - startRadius) * coefficient + startRadius).let {
                if (it > endRadius) {
                    endRadius
                } else {
                    it
                }
            }
            radiuses.add(radius)
        }


//        waveMatrix.postScale(
//            currentScale,
//            currentScale,
//            height / 2f,
//            width / 2f
//        )


//        canvas.concat(waveMatrix)

        for (i in 0 until waveCount) {
            canvas.drawCircle(centerX.toFloat(), centerY.toFloat(), radiuses[i], paints[i])
        }

        if (isAnimating) {
            postInvalidateDelayed(1000L / FRAMES_PER_SECOND)
        }
    }
}