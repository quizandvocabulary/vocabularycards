package me.asddsajpgnoob.quizandvocabulary.widget.quizCard

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.cardview.widget.CardView
import androidx.core.animation.addListener
import androidx.core.view.isVisible
import androidx.viewbinding.ViewBinding
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabulary.databinding.WidgetQuizCardFirstPageBinding
import me.asddsajpgnoob.quizandvocabulary.databinding.WidgetQuizCardSecondPageBinding
import me.asddsajpgnoob.quizandvocabulary.ui.common.model.PlaybackInfo
import me.asddsajpgnoob.quizandvocabulary.util.Constants
import me.asddsajpgnoob.quizandvocabulary.util.DateUtils
import me.asddsajpgnoob.quizandvocabulary.util.DisplayMeasureUtils
import me.asddsajpgnoob.quizandvocabulary.widget.quizCard.adapter.AudiosAdapter
import me.asddsajpgnoob.quizandvocabulary.widget.quizCard.adapter.ExamplesAdapter

@SuppressLint("ClickableViewAccessibility")
class QuizCardView(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    CardView(context, attrs, defStyleAttr), AudiosAdapter.AudiosAdapterListener {

    private val animationDuration: Int

    var isFirstVisible: Boolean
        private set

    private val showAdditionalData =
        DisplayMeasureUtils.getDeviceSizeDp(context).second >= Constants.MIN_DEVICE_HEIGHT_TO_SHOW_QUIZ_ADDITIONAL_DATA_DP

    private val firstBinding: WidgetQuizCardFirstPageBinding
    private val secondBinding: WidgetQuizCardSecondPageBinding

    private val examplesAdapter = ExamplesAdapter()
    private val audiosAdapter = AudiosAdapter(this)

    private var card: VocabularyCard? = null

    private var onPlayPauseListener: OnPlayPauseClickListener? = null

    private var quizCardAnimationListener: QuizCardAnimationListener? = null
    private var onShowMoreClickListener: OnShowMoreClickListener? = null
    private var onShowInQuizCheckedChangedListener: OnShowInQuizCheckedChangedListener? = null

    var isAnimating = false
        private set

    constructor(context: Context, attrs: AttributeSet?) : this(
        context,
        attrs,
        R.style.QuizCardCardViewStile
    )

    constructor(context: Context) : this(context, null)

    init {
        val attrsTypedArray = context.obtainStyledAttributes(attrs, R.styleable.QuizCardView)
        animationDuration =
            attrsTypedArray.getInt(R.styleable.QuizCardView_android_animationDuration, 1000)
        isFirstVisible = attrsTypedArray.getBoolean(R.styleable.QuizCardView_isFirstVisible, true)
        attrsTypedArray.recycle()

        val layoutInflater = LayoutInflater.from(context)
        firstBinding = WidgetQuizCardFirstPageBinding.inflate(layoutInflater, this, false)
        secondBinding = WidgetQuizCardSecondPageBinding.inflate(layoutInflater, this, false)

        firstBinding.root.isVisible = isFirstVisible
        secondBinding.root.isVisible = !isFirstVisible

        secondBinding.buttonShowMore.setOnClickListener {
            onShowMoreClickListener?.onShowMoreClicked()
        }
        secondBinding.checkBoxShowInQuiz.setOnCheckedChangeListener { _, isChecked ->
            onShowInQuizCheckedChangedListener?.onCheckedChange(isChecked)
        }

        addView(secondBinding.root)
        addView(firstBinding.root)
    }

    private val closeAnimator: ValueAnimator = ValueAnimator.ofFloat(1f, 0f)
        .apply {
            duration = animationDuration.toLong() / 2L
            addUpdateListener {
                val value = it.animatedValue as Float
                scaleX = value
            }
            addListener(
                onStart = {
                    isAnimating = true
                    quizCardAnimationListener?.onAnimationStart()
                },
                onEnd = {
                    isFirstVisible = !isFirstVisible
                    val frontChild = getChildViewBindingBySide(true).root
                    val behindChild = getChildViewBindingBySide(false).root

                    frontChild.isVisible = true
                    behindChild.isVisible = false

                    openAnimator.start()
                }
            )
        }

    private val openAnimator: ValueAnimator = ValueAnimator.ofFloat(0f, 1f)
        .apply {
            duration = animationDuration.toLong() / 2L
            addUpdateListener {
                val value = it.animatedValue as Float
                scaleX = value
            }
            addListener(
                onEnd = {
                    isAnimating = false
                    quizCardAnimationListener?.onAnimationEnd()
                }
            )
        }

    fun setData(playbackInfo: PlaybackInfo?, item: VocabularyCard) {
        this.card = item
        firstBinding.apply {
            textOrigin.text = item.origin
        }

        val maxExamples = if (showAdditionalData) {
            Constants.QUIZ_ADDITIONAL_DATA_EXAMPLES_COUNT
        } else {
            Constants.QUIZ_NORMAL_DATA_EXAMPLES_COUNT
        }
        val maxAudios = if (showAdditionalData) {
            Constants.QUIZ_ADDITIONAL_DATA_AUDIOS_COUNT
        } else {
            Constants.QUIZ_NORMAL_DATA_AUDIOS_COUNT
        }

        secondBinding.apply {
            textOrigin.text = item.origin
            textTranslation.text = item.translation
            checkBoxShowInQuiz.isChecked = item.showInQuiz
            textGuessedCount.text =
                context.getString(R.string.Guessed_count_colon_number, item.guessedCount)
            textNotGuessedCount.text =
                context.getString(R.string.Not_guessed_count_colon_number, item.notGuessedCount)

            val isExamplesMoreThanMax = item.examples.size > maxExamples
            val examplesItems = if (isExamplesMoreThanMax) {
                val result = mutableListOf<String>()
                for (i in item.examples.indices) {
                    result.add(item.examples[i])
                    if (i == maxExamples - 1) {
                        break
                    }
                }
                result
            } else {
                item.examples
            }

            val isAudiosMoreThanMax = item.audioFiles.size > maxAudios
            val audioItems = if (isAudiosMoreThanMax) {
                val result = mutableListOf<String>()
                for (i in item.audioFiles.indices) {
                    result.add(item.audioFiles[i])
                    if (i == maxAudios - 1) {
                        break
                    }
                }
                result
            } else {
                item.audioFiles
            }

            recyclerViewExamples.adapter = examplesAdapter
            examplesAdapter.submitListData(examplesItems)
            recyclerViewExamples.suppressLayout(true)

            recyclerViewAudios.adapter = audiosAdapter
            audiosAdapter.submitListData(playbackInfo, audioItems)
            recyclerViewAudios.suppressLayout(true)
        }
    }

    fun updatePlaybackInfo(playbackInfo: PlaybackInfo?) {
        audiosAdapter.updatePlaybackInfo(playbackInfo)
    }

    fun setSideForce(isFirst: Boolean) {
        closeAnimator.cancel()
        openAnimator.cancel()
        scaleX = 1f
        isFirstVisible = isFirst
        firstBinding.root.isVisible = isFirst
        secondBinding.root.isVisible = !isFirst
    }

    fun setSideIfNotAnimating(isFirst: Boolean) {
        if (!isAnimating) {
            setSideForce(isFirst)
        }
    }

    fun animateChangeSide() {
        if (isAnimating) {
            return
        }

        closeAnimator.start()
    }

    fun setQuizCardAnimationListener(
        onStart: () -> Unit = {},
        onEnd: () -> Unit = {}
    ) {
        setQuizCardAnimationListener(object : QuizCardAnimationListener {
            override fun onAnimationStart() {
                onStart.invoke()
            }

            override fun onAnimationEnd() {
                onEnd.invoke()
            }
        })
    }

    fun setQuizCardAnimationListener(listener: QuizCardAnimationListener?) {
        quizCardAnimationListener = listener
    }

    fun setOnPlayPauseListener(listener: OnPlayPauseClickListener?) {
        onPlayPauseListener = listener
    }

    fun setOnPlayPauseListener(function: (fileName: String) -> Unit) {
        setOnPlayPauseListener(object : OnPlayPauseClickListener {
            override fun onPlayPauseClicked(fileName: String) {
                function.invoke(fileName)
            }
        })
    }

    fun setOnShowMoreClickListener(listener: OnShowMoreClickListener?) {
        onShowMoreClickListener = listener
    }

    fun setOnShowMoreClickListener(function: () -> Unit) {
        onShowMoreClickListener = object : OnShowMoreClickListener {
            override fun onShowMoreClicked() {
                function.invoke()
            }
        }
    }

    fun setOnShowInQuizCheckedChangeListener(listener: OnShowInQuizCheckedChangedListener?) {
        onShowInQuizCheckedChangedListener = listener
    }

    fun setOnShowInQuizCheckedChangeListener(function: (isChecked: Boolean) -> Unit) {
        onShowInQuizCheckedChangedListener = object : OnShowInQuizCheckedChangedListener {
            override fun onCheckedChange(isChecked: Boolean) {
                function.invoke(isChecked)
            }
        }
    }

    private fun getChildViewBindingBySide(front: Boolean): ViewBinding {
        return if (isFirstVisible) {
            if (front) {
                firstBinding
            } else {
                secondBinding
            }
        } else {
            if (front) {
                secondBinding
            } else {
                firstBinding
            }
        }
    }

    override fun onPlayPauseClicked(fileName: String) {
        onPlayPauseListener?.onPlayPauseClicked(fileName)
    }

    interface QuizCardAnimationListener {
        fun onAnimationStart()
        fun onAnimationEnd()
    }

    @FunctionalInterface
    interface OnPlayPauseClickListener {
        fun onPlayPauseClicked(fileName: String)
    }

    @FunctionalInterface
    interface OnShowMoreClickListener {
        fun onShowMoreClicked()
    }

    @FunctionalInterface
    interface OnShowInQuizCheckedChangedListener {
        fun onCheckedChange(isChecked: Boolean)
    }
}