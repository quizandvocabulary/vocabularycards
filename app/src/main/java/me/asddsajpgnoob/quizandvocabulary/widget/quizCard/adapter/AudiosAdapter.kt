package me.asddsajpgnoob.quizandvocabulary.widget.quizCard.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import me.asddsajpgnoob.quizandvocabulary.R
import me.asddsajpgnoob.quizandvocabulary.databinding.ItemAudioBinding
import me.asddsajpgnoob.quizandvocabulary.ui.base.BaseViewHolder
import me.asddsajpgnoob.quizandvocabulary.ui.common.model.PlaybackInfo

class AudiosAdapter(
    private val listener: AudiosAdapterListener
) : RecyclerView.Adapter<AudiosAdapter.AudioViewHolder>() {

    private var currentPlaybackInfo: PlaybackInfo? = null
    private var playingViewHolder: AudioViewHolder? = null

    private var items = mutableListOf<String>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AudioViewHolder {
        val binding =
            ItemAudioBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AudioViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AudioViewHolder, position: Int) =
        holder.bind(items[position])

    override fun getItemCount() = items.size

    override fun onViewRecycled(holder: AudioViewHolder) {
        super.onViewRecycled(holder)
        val pos = holder.adapterPosition
        if (pos != RecyclerView.NO_POSITION) {
            val item = items[holder.adapterPosition]
            val playingInfo = currentPlaybackInfo
            if (item == playingInfo?.fileName) {
                playingViewHolder?.updatePlaying(false, playingInfo.duration, 0)
                playingViewHolder = null
            }
        }
    }

    fun submitListData(cardAndPlaybackInfo: PlaybackInfo?, newItems: List<String>) {
        val oldItems = items.toList()
        items.clear()
        items.addAll(newItems)
        val diffCallback = AudioDiffCallback(oldItems, items, cardAndPlaybackInfo)
        val diffResult = DiffUtil.calculateDiff(diffCallback, false)
        diffResult.dispatchUpdatesTo(this)
    }

    fun updatePlaybackInfo(newPlaybackInfo: PlaybackInfo?) {
        if (newPlaybackInfo == null) {
            playingViewHolder?.updatePlaying(false, currentPlaybackInfo?.duration ?: 100, 0)
        } else {
            playingViewHolder?.updatePlaying(
                newPlaybackInfo.playing,
                newPlaybackInfo.duration,
                newPlaybackInfo.playingPosition
            )
        }
        currentPlaybackInfo = newPlaybackInfo
    }

    inner class AudioViewHolder(binding: ItemAudioBinding) :
        BaseViewHolder<String, ItemAudioBinding>(binding) {

        init {
            binding.apply {
                audioSeekBar.isEnabled = false
                imagePlayPause.setOnClickListener {
                    val pos = adapterPosition
                    if (pos != RecyclerView.NO_POSITION) {
                        playingViewHolder?.updatePlaying(false, 100, 0)
                        playingViewHolder = this@AudioViewHolder

                        listener.onPlayPauseClicked(items[pos])
                    }
                }
            }
        }

        fun updatePlaying(playing: Boolean, duration: Int, progress: Int) {
            @DrawableRes val playPauseIconRes: Int = if (playing) {
                R.drawable.ic_pause_circle_filled_orange_24
            } else {
                R.drawable.ic_play_circle_filled_orange_24
            }
            binding.apply {
                imagePlayPause.setImageResource(playPauseIconRes)
                audioSeekBar.max = duration
                audioSeekBar.progress = progress
            }
        }

        override fun bind(item: String) {
            val playingInfo = currentPlaybackInfo
            if (playingInfo == null) {
                updatePlaying(false, 100, 0)
                return
            }
            val playing = playingInfo.fileName == item
            if (playing) {
                playingViewHolder = this
                updatePlaying(playing, playingInfo.duration, playingInfo.playingPosition)
            } else {
                updatePlaying(playing, 100, 0)
            }
        }
    }

    class AudioDiffCallback(
        private val oldList: List<String>,
        private val newList: List<String>,
        private val cardAndInfoInfo: PlaybackInfo?
    ) : DiffUtil.Callback() {
        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition] == newList[newItemPosition]

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldList[oldItemPosition]
            val newItem = newList[newItemPosition]

            cardAndInfoInfo?.let {
                if (newItem == it.fileName || oldItem == it.fileName) {
                    return false
                }
            }

            return true
        }

    }

    interface AudiosAdapterListener {
        fun onPlayPauseClicked(fileName: String)
    }
}